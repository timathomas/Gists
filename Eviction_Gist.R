# ==========================================================================
# Counts of Evictions for 2004 to 2017
# ==========================================================================

# ==========================================================================
# Libraries
# ==========================================================================

	library(tidyverse)

# ==========================================================================
# Data
# ==========================================================================

	ev <- data.table::fread("R_Drive/Project/Evictions/Data/Output/Parties_2004_2017.csv")

# ==========================================================================
# Tables
# ==========================================================================

	counts <- ev %>%
				group_by(County, Year) %>%
				select(Number) %>%
				distinct() %>%
				summarize(count = n())

# ==========================================================================
# Plot
# ==========================================================================

	ggplot(counts, aes(x = Year, y = count, color = County)) +
		geom_line(size = 1) +
		theme_minimal() +
		theme(legend.position="none",
			  axis.text.x = element_text(angle = -45, hjust = 0),
				panel.grid.minor.x = element_blank()) +
		geom_text(data = counts %>% filter(Year == 2015),
				   aes(label = County,
				   	   x = Year + 1,
                        y = count),
				   nudge_y = 175,
				   nudge_x = -.5,
				   fontface = "bold",
					check_overlap = TRUE) +
		geom_text(data = counts %>% filter(Year %in% c(2005, 2007, 2009, 2011, 2013, 2017)),
				   aes(label = count,
				   	   x = Year + 1,
                        y = count),
				   nudge_y = 200,
				   nudge_x = -.7,
				   size = 3,
					check_overlap = TRUE) +
		labs(title = "Unlawful Detainer Cases by Washington County",
				  y = "Count",
				  x = "Year") +
		scale_x_continuous(breaks=c(2004:2017))

ggsave(filename = "H_Drive/Academe/Presentations/180927_StateTestimony/WAUDCases.pdf",
	   width = 8,
	   height = 6)

