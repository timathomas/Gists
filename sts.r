# ==========================================================================
# Spatio Temporal Standardization
# ==========================================================================

library(tidyverse)
library(sf)

# ==========================================================================
# Pull in Data
# ==========================================================================

# ==========================================================================
	# sts with sf
	# ==========================================================================

				rm(list=ls()) #reset
			options(scipen=10) # avoid scientific notation

			# library(rgdal)
			# library(rgeos)
			library(sf)
			# library(cartography)
			library(mapview)
			library(tidycensus)
			options(tigris_use_cache = TRUE) # for caching shapefiles
		    library(tidyverse)

		# ==========================================================================
		# Pull in census data
		# ==========================================================================

		    racevars <- c(White = "B03002_003",
		              		Black = "B03002_004",
		              		Asian = "B03002_005",
		           			Latino = "B03002_012")

		    king <- get_acs(geography = "tract", variables = racevars, survey = "acs5", state = "wa", county = "king", geometry = TRUE, summary_var = "B03002_001", output = "wide")

		    king <- get_decennial(state = "WA",
					 	  county = "King",
					 	  geography = "county",
					 	  geometry = TRUE,
					 	  variables = "P0050003",
					 	  year = 2010) %>%
			st_transform(3857)


		# ==========================================================================
		# create grid overlaying geometry
		# ==========================================================================

		### Transform crs to get areal units for next steps
		    st_crs(king) # CRS of geom
		    king2 <- st_transform(king, 3857)
		    		# need meters: WGS 84 / Pseudo Mercator
		    		# Vignette: https://cran.r-project.org/web/packages/sf/vignettes/sf3.html
		    st_crs(king2)

		### Create grid overlaying geometry
		    	### See: https://cran.r-project.org/web/packages/sf/vignettes/sf5.html#graticules
		    avg <- mean(as.numeric(st_area(king2))) # for cellsize
		    grid <- st_make_grid(king2, cellsize = sqrt(avg)/2)
		    mapview(grid)
			plot(king2["BlackE"])
			plot(grid, add=T)

			a1 = st_interpolate_aw(king2 %>% select(ends_with("E")), grid, extensive = T)
			glimpse(a1)
				# Extensive = TRUE means the summed count is preserved.
				# Extensive = False means the means are preseversed.
			mapview(a1[2]) +
			mapview(king2["BlackE"])