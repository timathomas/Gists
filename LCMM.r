# Create Neighborhood Contexts data for analysis
# tim thomas
# 2/11/16

	rm(list=ls()) #reset
	options(scipen=10) # avoid scientific notation
	gc()

library(colorout)
library(tidyr)
library(rgdal)
# library(rgeos)
# library(maptools)
library(grid)
library(gridExtra)
library(dplyr)
# library(reshape2)
library(ggplot2)
library(ggthemes)
library(maptools)
# library(seg)
# library(spdep)
library(wesanderson)
library(lcmm)

# load in data
	# NCDB
	load("/Users/timothythomas/Academe/Data/NCDB/Raw/NCDB_1970_2010.RData")

	# load spatial data
	# sea.tracts <- readOGR("/Users/timothythomas/Academe/Google Drive/UW/Data/USCensus/Shapefiles/2010/Washington/Seattle","SeaTr2010", stringsAsFactors=F)
	sea.tracts <- readOGR("/Users/timothythomas/Academe/Research/SeattleCrimeProject/Data/Shapefiles","SeaSelect2010Tracts", stringsAsFactors=F)
	wa.tracts <- readOGR("/Users/timothythomas/Academe/Data/USCensus/Shapefiles/2010/Washington/Tracts","WA2010Tracts", stringsAsFactors=F)

# Get WA and Seattle GEOID10 id's
	seageo <- sea.tracts$GEOID10
	wageo <- wa.tracts$GEOID10

# Subset NCDB for WA
	ncdb.wa <- as.data.frame(ncdb[ncdb$GEO2010 %in% wageo, ]) # Subset
	# glimpse(ncdb.wa)

	rm(ncdb)
# Create Disadvantage Measure for Seattle
		dis <- ncdb.wa %>%
				group_by(GEO2010) %>%
						# 1980
				select(welf80=WELFARE8, # prop welfare
						pov80=POVRAT8, # prop poverty
						unemp80=UNEMPRT8, # prop unemployed
						femh80=FFH8, # prop female headed w/ children
						child80=CHILD8, # prop under 18
						# 1990
						welf90=WELFARE9,
						pov90=POVRAT9,
						unemp90=UNEMPRT9,
						femh90=FFH9,
						child90=CHILD9,
						# 2000
						welf00=WELFARE0,
						pov00=POVRAT0,
						unemp00=UNEMPRT0,
						femh00=FFH0,
						child00=CHILD0,
						# 2010
						welf10=WELFAR1AR,
						pov10=POVRAT1A,
						unemp10=UNEMPRT1A,
						femh10=FFH1A,
						child10=CHILD1A)

	dis[dis == -999.000] <- NA #make -999 (missing data) NAs
	summary(dis)
# Subset only Seattle Tracts
	subsea <- as.data.frame(dis[dis$GEO2010 %in% seageo, ])
	# plot(sort(subsea$unemp), type="l")

	sea.dis <- data.frame(subsea[1],lapply(subsea[2:21],scale)) %>%
				group_by(GEO2010) %>%
				summarise(dis1980=sum(welf80,pov80,unemp80,femh80,child80, na.rm=T),
						dis1990=sum(welf90,pov90,unemp90,femh90,child90, na.rm=T),
						dis2000=sum(welf00,pov00,unemp00,femh00,child00, na.rm=T),
						dis2010=sum(welf10,pov10,unemp10,femh10,child10, na.rm=T))
summary(sea.dis)

# Create Segregation measures for all US tracts with white, black and other.
		race <- ncdb.wa %>%
		group_by(GEO2010) %>%
		summarise(tot1980=SHR8D, #total pop for race/ethnicity
			nwt1980=SHRNHW8N, #Total non-Hisp./Latino White population
			nbl1980=SHRNHB8N, #Total non-Hisp./Latino Black/Afr. Am. population
			not1980=ifelse((tot1980-sum(nwt1980,nbl1980))<=0, 0, (tot1980-sum(nwt1980,nbl1980))), #Total nonhisp other

			pwt1980=SHRNHW8, #Prop. non-Hisp./Latino White population
			pbl1980=SHRNHB8, #Prop. non-Hisp./Latino Black/Afr. Am. population
			pot1980=(ifelse(not1980 >0,not1980/tot1980,0)), #Prop. non-Hisp other

			tot1990=SHR9D,
			nwt1990=SHRNHW9N,
			nbl1990=SHRNHB9N,
			not1990=ifelse((SHR9D-sum(SHRNHW9N,SHRNHB9N))<=0,0,(SHR9D-sum(SHRNHW9N,SHRNHB9N))),

			pwt1990=SHRNHW9,
			pbl1990=SHRNHB9,
			pot1990=(ifelse(not1990 > 0,not1990/tot1990,0)),

			tot2000=SHR0D,
			nwt2000=SHRNHW0N,
			nbl2000=SHRNHB0N,
			not2000=ifelse((SHR0D-sum(SHRNHW0N,SHRNHB0N))<=0,0,(SHR0D-sum(SHRNHW0N,SHRNHB0N))),

			pwt2000=SHRNHW0,
			pbl2000=SHRNHB0,
			pot2000=(ifelse(not2000 > 0, not2000/tot2000,0)),

			tot2010=SHR1D,
			nwt2010=SHRNHW1N,
			nbl2010=SHRNHB1N,
			not2010=ifelse((SHR1D-sum(SHRNHW1N,SHRNHB1N))<=0,0,(SHR1D-sum(SHRNHW1N,SHRNHB1N))),

			pwt2010=SHRNHW1,
			pbl2010=SHRNHB1,
			pot2010=(ifelse(not2010>0,not2010/tot2010,0)))

		summary(race)

sea.race <- as.data.frame(race[race$GEO2010 %in% seageo, ])

# Create dataset for LCMM
	sea.data <- left_join(sea.dis,sea.race)

#######################
# LCMM Categorization #
#######################

### LCMM
	## Prep data
	l.dt <- sea.data %>%
			mutate(GEO2010=factor(GEO2010)) %>%
			gather(v,value,dis1980:pot2010) %>%
			separate(v,c("var","year"),sep=3)%>%
			arrange(GEO2010) %>%
			spread(var,value) %>%
			as.data.frame()
	l.dt$GEO2010 <- as.character(l.dt$GEO2010)
	# l.dt$year <- as.factor(l.dt$year)


### Model: dis, black, and other ###

	# s2 <- multlcmm(dis+pbl+pot~1+year,random=~1+year,subject="GEO2010",link="linear",ng=2,mixture=~1+year,data=l.dt)
	s3 <- multlcmm(dis+pbl+pot~1+year,random=~1+year,subject="GEO2010",link="linear",ng=3,mixture=~1+year,data=l.dt)
	s4 <- multlcmm(dis+pbl+pot~1+year,random=~1+year,subject="GEO2010",link="linear",ng=4,mixture=~1+year,data=l.dt)
	s5 <- multlcmm(dis+pbl+pot~1+year,random=~1+year,subject="GEO2010",link="linear",ng=5,mixture=~1+year,data=l.dt)
	# s6 <- multlcmm(dis+pbl+pot~1+year,random=~1+year,subject="GEO2010",link="linear",ng=6,mixture=~1+year,data=l.dt)
	# s7 <- multlcmm(dis+pbl~1+year,random=~1+year,subject="GEO2010",link="linear",
	# 			ng=7,mixture=~1+year,data=l.dt)
	# s8 <- multlcmm(dis+pbl~1+year,random=~1+year,subject="GEO2010",link="linear",
	# 			ng=8,mixture=~1+year,data=l.dt)
	# s9 <- multlcmm(dis+pbl~1+year,random=~1+year,subject="GEO2010",link="linear",
	# 			ng=9,mixture=~1+year,data=l.dt)

		# s2$BIC # 251
		s3$BIC # 163 *
		s4$BIC # 144 ***
		s5$BIC # 173 *
		# s6$BIC # 193
		# s7$BIC #
		# s8$BIC #
		# s9$BIC #

		# Class 6 has lowest BIC

		summary(s3)
		postprob(s3)
		plot(s3,which="linkfunction")

		summary(s4)
		postprob(s4)
		plot(s4,which="linkfunction")

		summary(s5)
		postprob(s5)
		plot(s5,which="linkfunction")

		### CHOOSE S3: Though S4 has lower BIC, it produces NA in one of the columns

### Model dis + pbl = 1+year ###
	t2 <- multlcmm(dis+pbl~1+year,random=~1+year,subject="GEO2010",link="linear",ng=2,mixture=~1+year,data=l.dt)
	t3 <- multlcmm(dis+pbl~1+year,random=~1+year,subject="GEO2010",link="linear",ng=3,mixture=~1+year,data=l.dt)
	# t4 <- multlcmm(dis+pbl~1+year,random=~1+year,subject="GEO2010",link="linear",ng=4,mixture=~1+year,data=l.dt)
	# t5 <- multlcmm(dis+pbl~1+year,random=~1+year,subject="GEO2010",link="linear",ng=5,mixture=~1+year,data=l.dt)
	t6 <- multlcmm(dis+pbl~1+year,random=~1+year,subject="GEO2010",link="linear",ng=6,mixture=~1+year,data=l.dt)
	# t7 <- multlcmm(dis+pbl~1+year,random=~1+year,subject="GEO2010",link="linear",ng=7,mixture=~1+year,data=l.dt)
	# t8 <- multlcmm(dis+pbl~1+year,random=~1+year,subject="GEO2010",link="linear",ng=8,mixture=~1+year,data=l.dt)
	# s9 <- multlcmm(dis+pbl~1+year,random=~1+year,subject="GEO2010",link="linear",
	# 			ng=9,mixture=~1+year,data=l.dt)

		t2$BIC # 845 *
		t3$BIC # 856 *
		# t4$BIC # 837 NA's
		# t5$BIC # 906
		t6$BIC # 727 ***
		# t7$BIC # 934
		# t8$BIC # 981
		# s9$BIC #

		# Class 6 has lowest BIC

		summary(t3)
		postprob(t3)
		plot(t3,which="linkfunction")

		summary(t4)
		postprob(t4)
		plot(t4,which="linkfunction")

		summary(t6)
		postprob(t6)
		plot(t6,which="linkfunction")
		### CHOOSE t6: Lowest BIC by far


### Model: white, black, and other ###

	u2 <- multlcmm(pwt+pbl+pot~1+year,random=~1+year,subject="GEO2010",link="linear",ng=2,mixture=~1+year,data=l.dt)
	u3 <- multlcmm(pwt+pbl+pot~1+year,random=~1+year,subject="GEO2010",link="linear",ng=3,mixture=~1+year,data=l.dt)
	u4 <- multlcmm(pwt+pbl+pot~1+year,random=~1+year,subject="GEO2010",link="linear",ng=4,mixture=~1+year,data=l.dt)
	# u5 <- multlcmm(pwt+pbl+pot~1+year,random=~1+year,subject="GEO2010",link="linear",ng=5,mixture=~1+year,data=l.dt)
	# u6 <- multlcmm(pwt+pbl+pot~1+year,random=~1+year,subject="GEO2010",link="linear",ng=6,mixture=~1+year,data=l.dt)
	# u7 <- multlcmm(pwt+pbl~1+year,random=~1+year,subject="GEO2010",link="linear",ng=7,mixture=~1+year,data=l.dt)
	# u8 <- multlcmm(pwt+pbl~1+year,random=~1+year,subject="GEO2010",link="linear",ng=8,mixture=~1+year,data=l.dt)
	# u9 <- multlcmm(pwt+pbl~1+year,random=~1+year,subject="GEO2010",link="linear",ng=9,mixture=~1+year,data=l.dt)

		u2$BIC # -2715.636 *
		u3$BIC # -2771.443 ***
		u4$BIC # -2730.231 *
		# u5$BIC # -2705.915
		# u6$BIC # -2700.598
		# u7$BIC # -2023.527
		# u8$BIC # -2059.021
		# u9$BIC # -1990.252

		# Class 6 has lowest BIC

		summary(u2)
		postprob(u2)
		plot(u2,which="linkfunction")

		summary(u3)
		postprob(u3)
		plot(u3,which="linkfunction")

		summary(u4)
		postprob(u4)
		plot(u4,which="linkfunction")

		### CHOOSE u3: Lowest BIC by far


### Single value black proportion ###

############################
### Mapping and plotting ###
############################

#####################################
# Set up data and join to dataframe #
#####################################

	# GGPLOT of trajectories
	# Create df with GEO2010 and Category id's
		dbo3 <- as.data.frame(s3$pprob[,1:2]) #
		dbo4 <- as.data.frame(s4$pprob[,1:2])
		dbo5 <- as.data.frame(s5$pprob[,1:2])
		db2 <- as.data.frame(t2$pprob[,1:2])
		db3 <- as.data.frame(t3$pprob[,1:2])
		db6 <- as.data.frame(t6$pprob[,1:2]) #
		wbo2 <- as.data.frame(u2$pprob[,1:2])
		wbo3 <- as.data.frame(u3$pprob[,1:2]) #
		wbo4 <- as.data.frame(u4$pprob[,1:2])
		l.dtlcmm <- left_join(l.dt,dbo3) %>% rename(dbo3=class) %>% mutate(dbo3 = as.factor(dbo3))
		l.dtlcmm <- left_join(l.dtlcmm,dbo4) %>% rename(dbo4=class) %>% mutate(dbo4 = as.factor(dbo4))
		l.dtlcmm <- left_join(l.dtlcmm,dbo5) %>% rename(dbo5=class) %>% mutate(dbo5 = as.factor(dbo5))
		l.dtlcmm <- left_join(l.dtlcmm,db2) %>% rename(db2=class) %>% mutate(db2 = as.factor(db2))
		l.dtlcmm <- left_join(l.dtlcmm,db3) %>% rename(db3=class) %>% mutate(db3 = as.factor(db3))
		l.dtlcmm <- left_join(l.dtlcmm,db6) %>% rename(db6=class) %>% mutate(db6 = as.factor(db6))
		l.dtlcmm <- left_join(l.dtlcmm,wbo2) %>% rename(wbo2=class) %>% mutate(wbo2 = as.factor(wbo2))
		l.dtlcmm <- left_join(l.dtlcmm,wbo3) %>% rename(wbo3=class) %>% mutate(wbo3 = as.factor(wbo3))
		l.dtlcmm <- left_join(l.dtlcmm,wbo4) %>% rename(wbo4=class) %>% mutate(wbo4 = as.factor(wbo4))

		glimpse(l.dtlcmm)
		summary(l.dtlcmm)

##################################################
### Create GGPLOT of each cat with highest BIC ###
##################################################

### A way to think about these models is that within these tract categories produced by the multi-lcmm, disadvantage and percent black in each of these tracts was reacting in the following fashion that is shown in the graphs.

	# dbo3: disadvantage, percent black and percent other
	# Cat 3 on Disadvantage
	dis.dbo3 <- ggplot(l.dtlcmm, aes(year, dis, group=GEO2010, colour=dbo3)) +
			geom_line(alpha = .2) +
			geom_smooth(aes(group=dbo3), method="loess", size=2, se=T) +
			theme_classic() +
			labs(x = "Year", y = "Disadvantage Index", fill = "LCMM Cats") +
			theme(legend.position = "none") +
			ggtitle("Disadvantage Trajectories in These Tracts")
			# scale_color_manual(values = wes_palette("Darjeeling"))
	dis.dbo3

	# Cat 3 on percent black
	pbl.dbo3 <- ggplot(l.dtlcmm, aes(year, pbl, group=GEO2010, colour=dbo3)) +
			geom_line(alpha = .2) +
			geom_smooth(aes(group=dbo3), method="loess", size=2, se=T) +
			theme_classic() +
			theme(legend.position = "none") +
			labs(x = "Year", y = "Percent Black", fill = "LCMM Cats") +
			ggtitle("Percent Black Trajectories in These Tracts")
			# scale_color_manual(values = wes_palette("Darjeeling"))
	pbl.dbo3

	# Map
	dbo3.sp <- dbo3 %>% group_by(GEO2010)
	sea.lcmm <- sea.tracts
	sea.lcmm@data <- left_join(sea.lcmm@data,dbo3.sp, by = c("GEOID10" = "GEO2010"))
	sea.lcmm.f <- fortify(sea.lcmm,region="GISJOIN")
	sea.lcmm.f <- inner_join(sea.lcmm.f,sea.lcmm@data, by=c("id" = "GISJOIN")) %>% mutate(class = as.factor(class))
	Map <- ggplot(sea.lcmm.f, aes(long, lat, group = group, fill = class))
	map.dbo3 <- Map + geom_polygon() +
			coord_equal() +
			theme_map() +
			theme(legend.position = "right") +
			labs(fill = "LCMM Category") #+
			# theme(axis.text.x = element_text(angle = -45, hjust = 0)) +
			# labs(x = "Easting", y = "Northing", fill = "LCMM Cats") +
			# ggtitle("3 Category: dis+pbl+pot~1+year")

# Multiplot
dbo3.plot <- grid.arrange(map.dbo3,arrangeGrob(dis.dbo3,pbl.dbo3),ncol=2,top=textGrob("MultiLCMM/3 category: Disad. + %Black + %Other ~ 1+Year",gp=gpar(fontsize=25,font=2)))
# ggsave(dbo3.plot,file="/Users/timothythomas/Google Drive/UW/Research/SeattleCrimeProject/Documents/Images/160321_DBOcat3.pdf", scale=1.5)

##########################
# disadvantage and black #
##########################

### Cat 6 ###
	# Cat 6 on disadvantage
	dis.db6 <- ggplot(l.dtlcmm, aes(year, dis, group=GEO2010, colour=db6)) +
			geom_line(alpha = .2) +
			geom_smooth(aes(group=db6), method="loess", size=2, se=T) +
			theme_classic() +
			labs(x = "Year", y = "Disadvantage Index", fill = "LCMM Cats") +
			theme(legend.position = "none") +
			ggtitle("Disadvantage Trajectories in These Tracts")
			# scale_color_manual(values = wes_palette("Darjeeling"))
	dis.db6

	# Cat 6 on percent black
	pbl.db6 <- ggplot(l.dtlcmm, aes(year, pbl, group=GEO2010, colour=db6)) +
			geom_line(alpha = .2) +
			geom_smooth(aes(group=db6), method="loess", size=2, se=T) +
			theme_classic() +
			theme(legend.position = "none") +
			labs(x = "Year", y = "Percent Black", fill = "LCMM Cats") +
			ggtitle("Percent Black Trajectories in These Tracts")
			# scale_color_manual(values = wes_palette("Darjeeling"))
	pbl.db6

	# Map
	db6.sp <- db6 %>% group_by(GEO2010)
	sea.lcmm <- sea.tracts
	sea.lcmm@data <- left_join(sea.lcmm@data,db6.sp, by = c("GEOID10" = "GEO2010"))
	sea.lcmm.f <- fortify(sea.lcmm,region="GISJOIN")
	sea.lcmm.f <- inner_join(sea.lcmm.f,sea.lcmm@data, by=c("id" = "GISJOIN")) %>% mutate(class = as.factor(class))
	Map <- ggplot(sea.lcmm.f, aes(long, lat, group = group, fill = class))
	map.db6 <- Map + geom_polygon() +
			coord_equal() +
			theme_map() +
			theme(legend.position = "right") +
			labs(fill = "LCMM Category") #+
			# theme(axis.text.x = element_text(angle = -45, hjust = 0)) +
			# labs(x = "Easting", y = "Northing", fill = "LCMM Cats") +
			# ggtitle("6 Category: dis+pbl~1+year")

# Multiplot
db6.plot <- grid.arrange(map.db6,arrangeGrob(dis.db6,pbl.db6),ncol=2,top=textGrob("MultiLCMM/6 category: Disadv. + %Black ~ 1+Year",gp=gpar(fontsize=25,font=2)))
# ggsave(db6.plot,file="/Users/timothythomas/Google Drive/UW/Research/SeattleCrimeProject/Documents/Images/160321_DBcat6.pdf", scale=1.5)
### This shows the Capitol hill displacement over 40 years as well as the increase in black in housing authority locations (West Seattle.

### Cat 3 ###
	# Cat 6 on disadvantage
	dis.db3 <- ggplot(l.dtlcmm, aes(year, dis, group=GEO2010, colour=db3)) +
			geom_line(alpha = .2) +
			geom_smooth(aes(group=db3), method="loess", size=2, se=T) +
			theme_classic() +
			labs(x = "Year", y = "Disadvantage Index", fill = "LCMM Cats") +
			theme(legend.position = "none") +
			ggtitle("Disadvantage Trajectories in These Tracts")
			# scale_color_manual(values = wes_palette("Darjeeling"))
	dis.db3

	# Cat 6 on percent black
	pbl.db3 <- ggplot(l.dtlcmm, aes(year, pbl, group=GEO2010, colour=db3)) +
			geom_line(alpha = .2) +
			geom_smooth(aes(group=db3), method="loess", size=2, se=T) +
			theme_classic() +
			theme(legend.position = "none") +
			labs(x = "Year", y = "Percent Black", fill = "LCMM Cats") +
			ggtitle("Percent Black Trajectories in These Tracts")
			# scale_color_manual(values = wes_palette("Darjeeling"))
	pbl.db3

	# Map
	db3.sp <- db3 %>% group_by(GEO2010)
	sea.lcmm <- sea.tracts
	sea.lcmm@data <- left_join(sea.lcmm@data,db3.sp, by = c("GEOID10" = "GEO2010"))
	sea.lcmm.f <- fortify(sea.lcmm,region="GISJOIN")
	sea.lcmm.f <- inner_join(sea.lcmm.f,sea.lcmm@data, by=c("id" = "GISJOIN")) %>% mutate(class = as.factor(class))
	Map <- ggplot(sea.lcmm.f, aes(long, lat, group = group, fill = class))
	map.db3 <- Map + geom_polygon() +
			coord_equal() +
			theme_map() +
			theme(legend.position = "right") +
			labs(fill = "LCMM Category") # +
			# theme(axis.text.x = element_text(angle = -45, hjust = 0)) +
			# labs(x = "Easting", y = "Northing", fill = "LCMM Cats") +
			# ggtitle("3 Category: dis+pbl~1+year")

# Multiplot
db3.plot <- grid.arrange(map.db3,arrangeGrob(dis.db3,pbl.db3),ncol=2, top=textGrob("MultiLCMM/3 category: Disadv. + %Black ~ 1+Year",gp=gpar(fontsize=25,font=2)))

# ggsave(db3.plot,file="/Users/timothythomas/Google Drive/UW/Research/SeattleCrimeProject/Documents/Images/160321_DBcat3.pdf", scale=1.5)
### In this model using three categories, we miss the important trajectory of black decline in Capitol hill and Madison valley. However, we do maintain some important declines in disadvantage.

##########################
# White, Black and Other #
##########################

# wbo3
### Cat 6 ###
	# Cat 6 on disadvantage
	dis.wbo3 <- ggplot(l.dtlcmm, aes(year, dis, group=GEO2010, colour=wbo3)) +
			geom_line(alpha = .2) +
			geom_smooth(aes(group=wbo3), method="loess", size=2, se=T) +
			theme_classic() +
			labs(x = "Year", y = "Disadvantage Index", fill = "LCMM Cats") +
			theme(legend.position = "none") +
			ggtitle("Disadvantage Trajectories in These Tracts")
			# scale_color_manual(values = wes_palette("Darjeeling"))
	dis.wbo3

	# Cat 6 on percent black
	pbl.wbo3 <- ggplot(l.dtlcmm, aes(year, pbl, group=GEO2010, colour=wbo3)) +
			geom_line(alpha = .2) +
			geom_smooth(aes(group=wbo3), method="loess", size=2, se=T) +
			theme_classic() +
			theme(legend.position = "none") +
			labs(x = "Year", y = "Percent Black", fill = "LCMM Cats") +
			ggtitle("Percent Black Trajectories in These Tracts")
			# scale_color_manual(values = wes_palette("Darjeeling"))
	pbl.wbo3

	# Map
	wbo3.sp <- wbo3 %>% group_by(GEO2010)
	sea.lcmm <- sea.tracts
	sea.lcmm@data <- left_join(sea.lcmm@data,wbo3.sp, by = c("GEOID10" = "GEO2010"))
	sea.lcmm.f <- fortify(sea.lcmm,region="GISJOIN")
	sea.lcmm.f <- inner_join(sea.lcmm.f,sea.lcmm@data, by=c("id" = "GISJOIN")) %>% mutate(class = as.factor(class))
	Map <- ggplot(sea.lcmm.f, aes(long, lat, group = group, fill = class))
	map.wbo3 <- Map + geom_polygon() +
			coord_equal() +
			theme_map() +
			theme(legend.position = "right") +
			labs(fill = "LCMM Category") #+
			# theme(axis.text.x = element_text(angle = -45, hjust = 0)) +
			# labs(x = "Easting", y = "Northing", fill = "LCMM Cats") +
			# ggtitle("6 Category: dis+pbl~1+year")

# Multiplot
wbo3.plot <- grid.arrange(map.wbo3,arrangeGrob(dis.wbo3,pbl.wbo3),ncol=2,top=textGrob("MultiLCMM/3 category: % White + %Black + %Other ~ 1+Year",gp=gpar(fontsize=25,font=2)))
# ggsave(wbo3.plot,file="/Users/timothythomas/Google Drive/UW/Research/SeattleCrimeProject/Documents/Images/160321_WBOcat3.pdf", scale=1.5)
### This shows the Capitol hill displacement over 40 years as well as the increase in black in housing authority locations (West Seattle.

save(l.dtlcmm, file="../Data/RData/160321_MLCMMNeighCat.RData")


##################################################
##################################################
##################################################

# # Visualize trajectory
# 	# GGPLOT of trajectories
# 	# Create df with GEO2010 and Category id's
# 		db3 <- as.data.frame(t3$pprob[,1:2])
# 		db4 <- as.data.frame(t4$pprob[,1:2])
# 		db6 <- as.data.frame(t6$pprob[,1:2])
# 		l.dtlcmm <- left_join(l.dt,db3) %>% rename(db3=class) %>% mutate(db3 = as.factor(db3))
# 		l.dtlcmm <- left_join(l.dtlcmm,db4) %>% rename(db4=class) %>% mutate(db4 = as.factor(db4))
# 		l.dtlcmm <- left_join(l.dtlcmm,db6) %>% rename(db6=class) %>% mutate(db6 = as.factor(db6))
# 		glimpse(l.dtlcmm)

# 	# Create GGPLOT of each cat
# 		# Cat 3 on Disadvantage
# 	dis6 <- ggplot(l.dtlcmm, aes(year, dis, group=GEO2010, colour=db6)) +
# 			geom_line(alpha = .2) +
# 			geom_smooth(aes(group=db6), method="loess", size=2, se=F) +
# 			theme_classic() +
# 			labs(x = "Year", y = "Disadvantage Index", fill = "LCMM Cats") +
# 			theme(legend.position = "none") +
# 			ggtitle("Seattle Disadvantage Trajectories")
# 			# scale_color_manual(values = wes_palette("Darjeeling"))
# 	dis6

# 		# Cat 3 on percent black
# 	pbl6 <- ggplot(l.dtlcmm, aes(year, pbl, group=GEO2010, colour=db6)) +
# 			geom_line(alpha = .2) +
# 			geom_smooth(aes(group=db6), method="loess", size=2, se=F) +
# 			theme_classic() +
# 			theme(legend.position = "none") +
# 			labs(x = "Year", y = "Percent Black", fill = "LCMM Cats") +
# 			ggtitle("Seattle Percent Black Trajectories")
# 			# scale_color_manual(values = wes_palette("Darjeeling"))
# 	pbl6

# 	# Map
# 	db6.sp <- db6 %>% group_by(GEO2010)
# 	sea.lcmm <- sea.tracts
# 	sea.lcmm@data <- left_join(sea.lcmm@data,db6.sp, by = c("GEOID10" = "GEO2010"))
# 	sea.lcmm.f2 <- fortify(sea.lcmm,region="GISJOIN")
# 	sea.lcmm.f2 <- inner_join(sea.lcmm.f2,sea.lcmm@data, by=c("id" = "GISJOIN")) %>% mutate(class = as.factor(class))
# 	Map <- ggplot(sea.lcmm.f2, aes(long, lat, group = group, fill = class))
# 	cat6map <- Map + geom_polygon() +
# 			coord_equal() +
# 			theme_map() +
# 			theme(legend.position = "right") +
# 			labs(fill = "LCMM Category")
# 			# theme(axis.text.x = element_text(angle = -45, hjust = 0)) +
# 			# labs(x = "Easting", y = "Northing", fill = "LCMM Cats") +
# # 			ggtitle("3 Category: dis+pbl+pot~1+year")
# grid.arrange(cat6map,arrangeGrob(dis6,pbl6),ncol=2)
# 		# subset GEOID10 and class name 1:5
# 		# dbo4 <- as.data.frame(s4$pprob[,1:2]) # subset GEOID10 and class name 1:5
# 		dbo6 <- as.data.frame(s6$pprob[,1:2])
# 		l.dt <- left_join(l.dt,dbo3) %>% rename(class3=class) %>% mutate(class3.f=as.factor(class3))
# 		# l.dt <- left_join(l.dt,dbo4) %>% rename(class4=class) %>% mutate(class4.f=as.factor(class4))
# 		l.dt <- left_join(l.dt,dbo6) %>% rename(class6=class) %>% mutate(class6.f=as.factor(class6))


# 		# factor(lcmm3$class[sapply(l.dt$GEOID10, function(x) which(lcmm3$GEOID10==x))]) # Add class category to

# 	dis3 <- ggplot(l.dt, aes(year, dis, group=GEO2010, colour=class3.f)) +
# 				geom_line() +
# 				theme_classic() +
# 				# geom_smooth(aes(group=GEO2010, coulour=class3.f), size=0.5, se=F) +
# 				geom_smooth(aes(group=class3.f), method="loess", size=2, se=T) +
# 				scale_color_manual(values = wes_palette("Darjeeling"))

# 	dis4 <- ggplot(l.dt, aes(year, dis, group=GEO2010, colour=class4.f)) +
# 				geom_jitter() +
# 				theme_classic() +
# 				# geom_smooth(aes(group=GEO2010, coulour=class4.f), size=0.5, se=F) +
# 				geom_smooth(aes(group=class4.f), method="loess", size=2, se=T) +
# 				scale_color_manual(values = wes_palette("Darjeeling"))

# 	dis6 <- ggplot(l.dt, aes(year, dis, group=GEO2010, colour=class6.f)) +
# 					# geom_jitter() +
# 					geom_line() +
# 					theme_classic() +
# 					geom_smooth(aes(group=class6.f), method="loess", size=2, se=F) +
# 					labs(title="Dis")

# 	pbl6 <- ggplot(l.dt, aes(year, pbl, group=GEO2010, colour=class6.f)) +
# 					# geom_jitter() +
# 					geom_line() +
# 					theme_classic() +
# 					geom_smooth(aes(group=class6.f), method="loess", size=2, se=F) +
# 					labs(title="Pbl")
# 					# scale_color_manual(values = wes_palette("Zissou"))

# grid.arrange(dis6,pbl6,ncol=2,main="dis + pbl ~ 1+year")

# 					# scale_y_continuous()
# 					labs(x="Year",y="Disadvantage Index",colour="Latent Class", title="LCMM of Disadvantage") +
# 					scale_colour_discrete(labels=c("1: N=14 (9.59%)", "2: N=80 (54.79%)", "3: N=11 (7.53%)","4: N=33 (22.6%)","5: N=8  (5.48%)"))

# ### dis, black, adn other ###
# 	s2dpo <- multlcmm(dis+pbl+pot~1+year,random=~1+year,subject="GEO2010",link="linear",ng=2,mixture=~1+year,data=l.dt)
# 	s3dpo <- multlcmm(dis+pbl+pot~1+year,random=~1+year,subject="GEO2010",link="linear",ng=3,mixture=~1+year,data=l.dt)
# 	s4dpo <- multlcmm(dis+pbl+pot~1+year,random=~1+year,subject="GEO2010",link="linear",ng=4,mixture=~1+year,data=l.dt)
# 	s5dpo <- multlcmm(dis+pbl+pot~1+year,random=~1+year,subject="GEO2010",link="linear",ng=5,mixture=~1+year,data=l.dt)
# 	s6dpo <- multlcmm(dis+pbl+pot~1+year,random=~1+year,subject="GEO2010",link="linear",ng=6,mixture=~1+year,data=l.dt)
# 	# s7 <- multlcmm(dis+pbl~1+year,random=~1+year,subject="GEO2010",link="linear",
# 	# 			ng=7,mixture=~1+year,data=l.dt)
# 	# s8 <- multlcmm(dis+pbl~1+year,random=~1+year,subject="GEO2010",link="linear",
# 	# 			ng=8,mixture=~1+year,data=l.dt)
# 	# s9 <- multlcmm(dis+pbl~1+year,random=~1+year,subject="GEO2010",link="linear",
# 	# 			ng=9,mixture=~1+year,data=l.dt)

# 	summary(s4)
# 	postprob(s4)
# 	plot(s4,which="linkfunction")

# 		s2$BIC # 845
# 		s3$BIC # 857
# 		s4$BIC # 838
# 		s5$BIC # 907
# 		s6$BIC # 727
# 		s7$BIC # 934
# 		s8$BIC # 981
# 		s9$BIC # 953

# 		# Class 6 has lowest BIC

# 		summary(s6) # Lowest BIC
# 		postprob(s6) # this gives us the Ns and percentages of each class
# 		postprob(s3)

# # Visualize trajectory
# 		lcmm3 <- as.data.frame(s3$pprob[,1:2]) # subset GEOID10 and class name 1:5
# 		# lcmm4 <- as.data.frame(s4$pprob[,1:2]) # subset GEOID10 and class name 1:5
# 		lcmm6 <- as.data.frame(s6$pprob[,1:2])
# 		l.dt <- left_join(l.dt,lcmm3) %>% rename(class3=class) %>% mutate(class3.f=as.factor(class3))
# 		# l.dt <- left_join(l.dt,lcmm4) %>% rename(class4=class) %>% mutate(class4.f=as.factor(class4))
# 		l.dt <- left_join(l.dt,lcmm6) %>% rename(class6=class) %>% mutate(class6.f=as.factor(class6))


# 		# factor(lcmm3$class[sapply(l.dt$GEOID10, function(x) which(lcmm3$GEOID10==x))]) # Add class category to

# 	dis3 <- ggplot(l.dt, aes(year, dis, group=GEO2010, colour=class3.f)) +
# 				geom_line() +
# 				theme_classic() +
# 				# geom_smooth(aes(group=GEO2010, coulour=class3.f), size=0.5, se=F) +
# 				geom_smooth(aes(group=class3.f), method="loess", size=2, se=T) +
# 				scale_color_manual(values = wes_palette("Darjeeling"))


# # l.dt$GEO2010 <- as.character(l.dt$GEO2010)
# # lcmm3 <- as.data.frame(d2$pprob[,1:2])
# # l.dt$group2 <- factor(lcmm3$class[tapply(l.dt$GEO2010, function(x) which(lcmm3$GEO2010==x))])
# # p1 <- ggplot(l.dt, aes(x, y, group=GEO2010, colour=group2)) +
# # 		geom_line() +
# # 		geom_smooth(aes(group=group2), method="loess", size=2, se=F)  +
# # 		scale_y_continuous(limits = c(13,37)) +
# # 		labs(x="x",y="y",colour="Latent Class") +
# # 		opts(title="Raw")
# # p2 <- ggplot(dummy, aes(x, y, group=id, colour=group2)) +
# # 			geom_smooth(aes(group=id, colour=group2),size=0.5, se=F) +
# # 			geom_smooth(aes(group=group2), method="loess", size=2, se=T)  +
# # 			scale_y_continuous(limits = c(13,37)) +
# # 			labs(x="x",y="y",colour="Latent Class") +
# # 			opts(title="Smoothed", legend.position="none")
# # grid.arrange(p1,p2, ncol=2, main="2 Latent Class")

# # # m2 <- multlcmm(Ydep1+Ydep2~1+Time*X2,
# # # 				random=~1+Time,
# # # 				subject="ID",
# # # 				link="linear",
# # # 				ng=2,
# # # 				mixture=~1+Time,
# # # 				classmb=~1+X1,
# # # 				data=data_lcmm,
# # # 				B=c( 18,-20.77,1.16,-1.41,-1.39,-0.32,0.16,-0.26,1.69,1.12,1.1,10.8,1.24,24.88,1.89))

# # ### Plot lcmm
# # 	d2 <- as.data.frame(s4$pprob[,1:2]) # subset GEOID10 and class name 1:5
# # 	l.dt$group2 <- factor(d2$class[sapply(l.dt$GEOID10, function(x) which(d2$GEOID10==x))]) # Add class category to

# # 	l.dt5.plot <- ggplot(l.dt, aes(year, meas, group=GEOID10, colour=group5)) +
# # 					geom_smooth(aes(group=group5), method="loess", size=2, se=T) +
# # 					labs(x="Year",y="Disadvantage Index",colour="Latent Class", title="LCMM of Disadvantage") +
# # 					scale_colour_discrete(labels=c("1: N=14 (9.59%)", "2: N=80 (54.79%)", "3: N=11 (7.53%)","4: N=33 (22.6%)","5: N=8  (5.48%)"))

# # 	# Attempt 2 with just lcmm
# # 	d5 <-lcmm(dis~year,subject='GEO2010',mixture=~year,ng=5,idiag=T,data=l.dt,link="linear")

# # ### Do not run ###

# # m1 <- multlcmm(Ydep1+Ydep2~1+Time*X2+contrast(X2),
# # 				random=~1+Time,
# # 				subject="ID",
# # 				randomY=TRUE,
# # 				link=c("4-manual-splines","3-manual-splines"),
# # 				intnodes=c(8,12,25),
# # 				data=data_lcmm)

# # # to reduce the computation time, the same model is estimated using
# # # a vector of initial values
# # m1 <- multlcmm(Ydep1+Ydep2~1+Time*X2+contrast(X2),
# # 				random=~1+Time,
# # 				subject="ID",
# # 				randomY=TRUE,
# # 				link=c("4-manual-splines","3-manual-splines"),
# # 				intnodes=c(8,12,25),
# # 				data=data_lcmm,
# # 				B=c(-1.071, -0.192,  0.106, -0.005, -0.193,  1.012,  0.870,  0.881,0.000,  0.000, -7.520,  1.401,  1.607 , 1.908,  1.431,  1.082,-7.528,  1.135 , 1.454 , 2.328, 1.052))


# # 	# output of the model
# # 	summary(m1)
# # 	# estimated link functions
# # 	plot(m1,which="linkfunction")
# # 	# variation percentages explained by linear mixed regression
# # 	VarExpl(m1,data.frame(Time=0))

# # #### Heterogeneous latent process mixed model with linear link functions
# # #### and 2 latent classes of trajectory
# # m2 <- multlcmm(Ydep1+Ydep2~1+Time*X2,
# # 				random=~1+Time,subject="ID",
# # 				link="linear",
# # 				ng=2,
# # 				mixture=~1+Time,
# # 				classmb=~1+X1,
# # 				data=data_lcmm,B=c( 18,-20.77,1.16,-1.41,-1.39,-0.32,0.16,-0.26,1.69,1.12,1.1,10.8,1.24,24.88,1.89))
# # # summary of the estimation
# # summary(m2)
# # # posterior classification
# # postprob(m2)
# # # longitudinal predictions in the outcomes scales for a given profile of covariates
# # newdata <- data.frame(Time=seq(0,5,length=100),X1=rep(0,100),X2=rep(0,100),X3=rep(0,100))
# # predGH <- predictY(m2,newdata,var.time="Time",methInteg=0,nsim=20)
# # head(predGH)
# # ## End(Not run)


# # ####################
# # ### End Test Bed ###
# # ####################

# # # More detailed code located at /Users/timothythomas/Google Drive/UW/Research/SCP/R/Data_151229_DisadvantageNCDB.r
# # # Disadvantage

# # 	# LCMM for disadvantage categories

# # 		d5 <-lcmm(meas~year,subject='GEO2010',mixture=~year,ng=5,idiag=T,data=d.prop.l,link="linear")
# # 		# dis5t <- as.data.frame(d5$pprob[,1:2]) # subset GEO2010 and class name 1:5
# # 	# Visualize trajectory
# # 		dis5t <- as.data.frame(d5$pprob[,1:2]) # subset GEOID10 and class name 1:5
# # 		dis$group5 <- factor(dis5t$class[sapply(dis$GEOID10, function(x) which(dis5t$GEOID10==x))]) # Add class category to

# # 	dis5.plot <- ggplot(dis, aes(year, meas, group=GEOID10, colour=group5)) +
# # 					geom_smooth(aes(group=group5), method="loess", size=2, se=T) +
# # 					labs(x="Year",y="Disadvantage Index",colour="Latent Class", title="LCMM of Disadvantage") +
# # 					scale_colour_discrete(labels=c("1: N=14 (9.59%)", "2: N=80 (54.79%)", "3: N=11 (7.53%)","4: N=33 (22.6%)","5: N=8  (5.48%)"))
# # ### Dissimilarity ###
# # # Set up data for analysis to wide then long format
# # 	d.wb <- sea.f@data %>%
# # 		group_by(GEOID10) %>%
# # 		select(d.wb1980,d.wb1990,d.wb2000,d.wb2010) %>%
# # 		gather(year, meas, -GEOID10) %>% 	# reshape wide to long
# # 		separate(year,into=c("dis","year"),sep=4) %>%
# # 		arrange(GEOID10)

# # 		glimpse(d.wb)

# # 		d.wb5 <-lcmm(meas~year,subject='GEOID10',mixture=~year,ng=5,idiag=T,data=d.wb,link="4-equi-splines")

# # 		# d.wb5$BIC # Relativly low BIC but chosen for

# # 		d.wb5t <- as.data.frame(d.wb5$pprob[,1:2]) # subset GEOID10 and class name 1:5


# # ### interaction ###
# # # Set up data for analysis to wide then long format
# # 	int.wb <- sea.f@data %>%
# # 		group_by(GEOID10) %>%
# # 		select(int.wb1980,int.wb1990,int.wb2000,int.wb2010) %>%
# # 		gather(year, meas, -GEOID10) %>% 	# reshape wide to long
# # 		separate(year,into=c("int","year"),sep=6) %>%
# # 		arrange(GEOID10)

# # 		glimpse(int.wb)

# # 		int.wb6 <-lcmm(meas~year,subject='GEOID10',mixture=~year,ng=6,idiag=T,data=int.wb,link="4-equi-splines")

# # 		int.wb6t <- as.data.frame(int.wb6$pprob[,1:2])

# # ### Isolation ###
# # # Set up data for analysis to wide then long format
# # 	iso.b <- sea.f@data %>%
# # 		group_by(GEOID10) %>%
# # 		select(iso.b1980,iso.b1990,iso.b2000,iso.b2010) %>%
# # 		gather(year, meas, -GEOID10) %>% 	# reshape wide to long
# # 		separate(year,into=c("int","year"),sep=5) %>%
# # 		arrange(GEOID10)

# # 		glimpse(iso.b)

# # 		iso.b5 <-lcmm(meas~year,subject='GEOID10',mixture=~year,ng=5,idiag=T,data=iso.b,link="4-quant-splines")

# # 		iso.b5t <- as.data.frame(iso.b5$pprob[,1:2]) # subset GEOID10 and class name 1:5


# # ### Thiel's H - Entropy of multi-group ###
# # # Set up data for analysis to wide then long format
# # 	Hcalc <- sea.f@data %>%
# # 		group_by(GEOID10) %>%
# # 		select(Hcalc1980,Hcalc1990,Hcalc2000,Hcalc2010) %>%
# # 		gather(year, meas, -GEOID10) %>% 	# reshape wide to long
# # 		separate(year,into=c("int","year"),sep=5) %>%
# # 		arrange(GEOID10)

# # 		glimpse(Hcalc)

# # 		Hcalc5 <-lcmm(meas~year,subject='GEOID10',mixture=~year,ng=5,idiag=T,data=Hcalc,link="beta")
# # 		Hcalc5t <- as.data.frame(Hcalc5$pprob[,1:2]) # subset GEOID10 and class name 1:5


# # # Merge for final dataset
# # 	data <- merge(dis5t,d.wb5t,by="GEOID10")
# # 	data <- merge(data, int.wb6t, by="GEOID10")
# # 	# colnames(data) <- c("GEOID10","Disadvantage","Dissimilarity","Interaction")
# # 	data <- merge(data, iso.b5t, by="GEOID10")
# # 	data <- merge(data,Hcalc5t, by="GEOID10")
# # 	head(data)
# # 	colnames(data) <- c("GEOID10","Disadvantage","Dissimilarity","Interaction","Isolation","Entropy")
# # 	head(data)
# # 	str(data)

# # 	sea.f@data <- merge(sea.f@data, data, by="GEOID10")

# # # Seattle Neighborhood dataframe
# # 	seacontexts.df <- sea.f@data

# # # Washington demographic data
# # 	wa.f <- wa.tracts
# # 	wa.f@data <- wa.f@data[,c(-2:-12,-14,-15)]
# # 	wacontexts.df <- wa.f@data

# # # Save neighborhood data
# # save(seacontexts.df,wacontexts.df,file="../Data/RData/Data_160212_NeighContexts.RData")

# # glimpse(seacontexts.df)
# # glimpse(wacontexts.df)



# # #######################
# # # LCMM Categorization #
# # #######################

# # # Disadvantage
# # 	# Set up data for analysis to long format
# # 		dis <- sea.f@data %>%
# # 			group_by(GEOID10) %>%
# # 			select(dis1980,dis1990,dis2000,dis2010) %>%
# # 			gather(year, meas, -GEOID10) %>% 	# reshape wide to long
# # 			separate(year,into=c("dis","year"),sep=3) %>%
# # 			arrange(GEOID10)

# # 	# LCMM for disadvantage categories
# # 		d2 <-lcmm(meas~year,subject='GEOID10',mixture=~year,ng=2,idiag=T,data=dis,link="linear")
# # 		d3 <-lcmm(meas~year,subject='GEOID10',mixture=~year,ng=3,idiag=T,data=dis,link="linear")
# # 		d4 <-lcmm(meas~year,subject='GEOID10',mixture=~year,ng=4,idiag=T,data=dis,link="linear")
# # 		d5 <-lcmm(meas~year,subject='GEOID10',mixture=~year,ng=5,idiag=T,data=dis,link="linear")
# # 		d6 <-lcmm(meas~year,subject='GEOID10',mixture=~year,ng=6,idiag=T,data=dis,link="linear")
# # 		d7 <-lcmm(meas~year,subject='GEOID10',mixture=~year,ng=7,idiag=T,data=dis,link="linear")
# # 		d8 <-lcmm(meas~year,subject='GEOID10',mixture=~year,ng=8,idiag=T,data=dis,link="linear")
# # 		d9 <-lcmm(meas~year,subject='GEOID10',mixture=~year,ng=9,idiag=T,data=dis,link="linear")

# # 		# Choose model using lowest BIC, which is d5
# # 		d2$BIC
# # 		d3$BIC
# # 		d4$BIC
# # 		d5$BIC
# # 		d6$BIC
# # 		d7$BIC
# # 		d8$BIC
# # 		d9$BIC
# # 		# Of the 8 groupings, class 5 has the lowest BIC and we go with that.

# # 		summary(d5) # Lowest BIC
# # 		postprob(d5) # this gives us the Ns and percentages of each class

# # 	# Visualize trajectory
# # 		dis5t <- as.data.frame(d5$pprob[,1:2]) # subset GEOID10 and class name 1:5
# # 		dis$group5 <- factor(dis5t$class[sapply(dis$GEOID10, function(x) which(dis5t$GEOID10==x))]) # Add class category to

# # 	dis5.plot <- ggplot(dis, aes(year, meas, group=GEOID10, colour=group5)) +
# # 					geom_smooth(aes(group=group5), method="loess", size=2, se=T) +
# # 					labs(x="Year",y="Disadvantage Index",colour="Latent Class", title="LCMM of Disadvantage") +
# # 					scale_colour_discrete(labels=c("1: N=14 (9.59%)", "2: N=80 (54.79%)", "3: N=11 (7.53%)","4: N=33 (22.6%)","5: N=8  (5.48%)"))

# # 	# Based on the LCMM, there are five categories:
# # 		# Cat 1: N=14 (9.59%) "upper-mid to sharp decline in 1990"
# # 		# Cat 2: N=80 (54.79%) "Steady low"
# # 		# Cat 3: N=11 (7.53%) "Rise to slow drop in 2000"
# # 		# Cat 4: N=33 (22.6%) "Slow, steady inrease"
# # 		# Cat 5: N=8  (5.48%) "High to sharp decline in 1990"

# # ### Dissimilarity ###
# # # Set up data for analysis to wide then long format
# # 	d.wb <- sea.f@data %>%
# # 		group_by(GEOID10) %>%
# # 		select(d.wb1980,d.wb1990,d.wb2000,d.wb2010) %>%
# # 		gather(year, meas, -GEOID10) %>% 	# reshape wide to long
# # 		separate(year,into=c("dis","year"),sep=4) %>%
# # 		arrange(GEOID10)

# # 		glimpse(d.wb)

# # 	# do the LCMM calculations for classes 2:9 for changes in disadvantage. These segregation models require link spline, where there are 4 nodes. I'm assuming that these are equdistant
# # 	# d.wb2 <-lcmm(meas~year,subject='GEOID10',mixture=~year,ng=2,idiag=F,data=d.wb, link="4-equi-splines")
# # 	# d.wb3 <-lcmm(meas~year,subject='GEOID10',mixture=~year,ng=3,idiag=T,data=d.wb,link="4-equi-splines")
# # 	d.wb4 <-lcmm(meas~year,subject='GEOID10',mixture=~year,ng=4,idiag=T,data=d.wb,link="4-equi-splines")
# # 	d.wb5 <-lcmm(meas~year,subject='GEOID10',mixture=~year,ng=5,idiag=T,data=d.wb,link="4-equi-splines")
# # 	d.wb6 <-lcmm(meas~year,subject='GEOID10',mixture=~year,ng=6,idiag=T,data=d.wb,link="4-equi-splines")
# # 	# d.wb7 <-lcmm(meas~year,subject='GEOID10',mixture=~year,ng=7,idiag=T,data=d.wb,link="4-equi-splines")
# # 	# d.wb8 <-lcmm(meas~year,subject='GEOID10',mixture=~year,ng=8,idiag=T,data=d.wb,link="4-equi-splines")
# # 	# d.wb9 <-lcmm(meas~year,subject='GEOID10',mixture=~year,ng=9,idiag=T,data=d.wb,link="4-equi-splines")
# # 	# d.wb10 <-lcmm(meas~year,subject='GEOID10',mixture=~year,ng=10,idiag=T,data=d.wb,link="4-equi-splines")
# # 	# d.wb11 <-lcmm(meas~year,subject='GEOID10',mixture=~year,ng=11,idiag=T,data=d.wb,link="4-equi-splines")

# # 	# Choose model using lowest BIC, which is d5
# # 	# d.wb2$BIC
# # 	# d.wb3$BIC
# # 	d.wb4$BIC
# # 	d.wb5$BIC # Relativly low BIC but chosen for parsimony
# # 	d.wb6$BIC
# # 	# d.wb7$BIC
# # 	# d.wb8$BIC
# # 	# d.wb9$BIC
# # 	# d.wb10$BIC
# # 	# d.wb11$BIC
# # 	# Of the 8 groupings, class 4 has the lowest BIC and we go with that.

# # 	postprob(d.wb4)
# # 	postprob(d.wb5)
# # 	postprob(d.wb6)
# # 	# postprob(d.wb7)
# # # Visualize trajectory
# # 	# d.wb4t <- as.data.frame(d.wb4$pprob[,1:2])
# # 	d.wb5t <- as.data.frame(d.wb5$pprob[,1:2]) # subset GEOID10 and class name 1:5
# # 	# d.wb6t <- as.data.frame(d.wb6$pprob[,1:2])
# # 	# d.wb7t <- as.data.frame(d.wb7$pprob[,1:2])
# # 	# d.wb$group4 <- factor(d.wb4t$class[sapply(d.wb$GEOID10, function(x) which(d.wb4t$GEOID10==x))])
# # 	d.wb$group5 <- factor(d.wb5t$class[sapply(d.wb$GEOID10, function(x) which(d.wb5t$GEOID10==x))]) # Add class category to
# # 	# d.wb$group6 <- factor(d.wb6t$class[sapply(d.wb$GEOID10, function(x) which(d.wb6t$GEOID10==x))])
# # 	# d.wb$group7 <- factor(d.wb7t$class[sapply(d.wb$GEOID10, function(x) which(d.wb7t$GEOID10==x))])


# # 	# ggplot(d.wb, aes(year, meas, group=GEOID10, colour=group4)) +
# # 	# 			geom_smooth(aes(group=group4), method="loess", size=2, se=T) +
# # 	# 			labs(x="Year",y="Dissimilarity",colour="Latent Class", title="Black and White Dissimilarity")
# # d.wb5.plot <- ggplot(d.wb, aes(year, meas, group=GEOID10, colour=group5)) +
# # 				geom_smooth(aes(group=group5), method="loess", size=2, se=T) +
# # 				labs(x="Year",y="Dissimilarity",colour="Latent Class", title="Black and White Dissimilarity") +
# # 				scale_colour_discrete(labels=c("1: N=46 (31.51%)","2: N=12 (8.22%)","3: N=5 (3.42%)","4: N=48 (32.88%)","5: N=35 (23.97%)"))





# # 	# ggplot(d.wb, aes(year, meas, group=GEOID10, colour=group6)) +
# # 	# 			geom_smooth(aes(group=group6), method="loess", size=2, se=T) +
# # 	# 			labs(x="Year",y="Dissimilarity",colour="Latent Class", title="Black and White Dissimilarity")
# # 	# ggplot(d.wb, aes(year, meas, group=GEOID10, colour=group7)) +
# # 	# 			geom_smooth(aes(group=group7), method="loess", size=2, se=T) +
# # 	# 			labs(x="Year",y="Dissimilarity",colour="Latent Class", title="Black and White Dissimilarity")

# # ### interaction ###
# # # Set up data for analysis to wide then long format
# # 	int.wb <- sea.f@data %>%
# # 		group_by(GEOID10) %>%
# # 		select(int.wb1980,int.wb1990,int.wb2000,int.wb2010) %>%
# # 		gather(year, meas, -GEOID10) %>% 	# reshape wide to long
# # 		separate(year,into=c("int","year"),sep=6) %>%
# # 		arrange(GEOID10)

# # 		glimpse(int.wb)

# # 	# do the LCMM calculations for classes 2:9 for changes in disadvantage. These segregation models require link spline, where there are 4 nodes. I'm assuming that these are equdistant
# # 	int.wb2 <-lcmm(meas~year,subject='GEOID10',mixture=~year,ng=2,idiag=F,data=int.wb, link="4-equi-splines")
# # 	int.wb3 <-lcmm(meas~year,subject='GEOID10',mixture=~year,ng=3,idiag=T,data=int.wb,link="4-equi-splines")
# # 	int.wb4 <-lcmm(meas~year,subject='GEOID10',mixture=~year,ng=4,idiag=T,data=int.wb,link="4-equi-splines")
# # 	int.wb5 <-lcmm(meas~year,subject='GEOID10',mixture=~year,ng=5,idiag=T,data=int.wb,link="4-equi-splines")
# # 	int.wb6 <-lcmm(meas~year,subject='GEOID10',mixture=~year,ng=6,idiag=T,data=int.wb,link="4-equi-splines")
# # 	int.wb7 <-lcmm(meas~year,subject='GEOID10',mixture=~year,ng=7,idiag=T,data=int.wb,link="4-equi-splines")
# # 	# int.wb8 <-lcmm(meas~year,subject='GEOID10',mixture=~year,ng=8,idiag=T,data=int.wb,link="4-equi-splines")
# # 	# int.wb9 <-lcmm(meas~year,subject='GEOID10',mixture=~year,ng=9,idiag=T,data=int.wb,link="4-equi-splines")
# # 	# int.wb10 <-lcmm(meas~year,subject='GEOID10',mixture=~year,ng=10,idiag=T,data=int.wb,link="4-equi-splines")
# # 	# int.wb11 <-lcmm(meas~year,subject='GEOID10',mixture=~year,ng=11,idiag=T,data=int.wb,link="4-equi-splines")

# # 	# Choose model using lowest BIC, which is d5
# # 	int.wb2$BIC
# # 	int.wb3$BIC
# # 	int.wb4$BIC
# # 	int.wb5$BIC
# # 	int.wb6$BIC
# # 	int.wb7$BIC
# # 	int.wb8$BIC
# # 	int.wb9$BIC
# # 	int.wb10$BIC
# # 	int.wb11$BIC
# # 	# Of the 8 groupings, class 4 has the lowest BIC and we go with that.

# # 	postprob(int.wb4)
# # 	postprob(int.wb5)
# # 	postprob(int.wb6)
# # 	# postprob(int.wb7)
# # # Visualize trajectory
# # 	int.wb4t <- as.data.frame(int.wb4$pprob[,1:2])
# # 	int.wb5t <- as.data.frame(int.wb5$pprob[,1:2]) # subset GEOID10 and class name 1:5
# # 	# int.wb6t <- as.data.frame(int.wb6$pprob[,1:2])
# # 	# int.wb7t <- as.data.frame(int.wb7$pprob[,1:2])
# # 	int.wb$group4 <- factor(int.wb4t$class[sapply(int.wb$GEOID10, function(x) which(int.wb4t$GEOID10==x))])
# # 	int.wb$group5 <- factor(int.wb5t$class[sapply(int.wb$GEOID10, function(x) which(int.wb5t$GEOID10==x))]) # Add class category to
# # 	# int.wb$group6 <- factor(int.wb6t$class[sapply(int.wb$GEOID10, function(x) which(int.wb6t$GEOID10==x))])
# # 	# int.wb$group7 <- factor(int.wb7t$class[sapply(int.wb$GEOID10, function(x) which(int.wb7t$GEOID10==x))])


# # 	ggplot(int.wb, aes(year, meas, group=GEOID10, colour=group4)) +
# # 				geom_smooth(aes(group=group4), method="loess", size=2, se=T) +
# # 				labs(x="Year",y="Interaction",colour="Latent Class", title="Black and White Interaction")
# # 	ggplot(int.wb, aes(year, meas, group=GEOID10, colour=group5)) +
# # 				geom_smooth(aes(group=group5), method="loess", size=2, se=T) +
# # 				labs(x="Year",y="Interaction",colour="Latent Class", title="Black and White Interaction")

# # int.wb6.plot <- ggplot(int.wb, aes(year, meas, group=GEOID10, colour=group6)) +
# # 				geom_smooth(aes(group=group6), method="loess", size=2, se=T) +
# # 				labs(x="Year",y="Interaction",colour="Latent Class", title="Black and White Interaction") +
# # 				scale_colour_discrete(labels=c("1: N=16 (10.96%)","2: N=57 (39.04%)","3: N=11 (7.53%)","4: N=11 (7.53%)","5: N=39 (26.71%)", "6: N=12 (8.22%)"))
# # 	ggplot(int.wb, aes(year, meas, group=GEOID10, colour=group7)) +
# # 				geom_smooth(aes(group=group7), method="loess", size=2, se=T) +
# # 				labs(x="Year",y="Interaction",colour="Latent Class", title="Black and White Interaction")


# # ### Isolation ###
# # # Set up data for analysis to wide then long format
# # 	iso.b <- sea.f@data %>%
# # 		group_by(GEOID10) %>%
# # 		select(iso.b1980,iso.b1990,iso.b2000,iso.b2010) %>%
# # 		gather(year, meas, -GEOID10) %>% 	# reshape wide to long
# # 		separate(year,into=c("int","year"),sep=5) %>%
# # 		arrange(GEOID10)

# # 		glimpse(iso.b)

# # 	# do the LCMM calculations for classes 2:9 for changes in disadvantage. These segregation models require link spline, where there are 4 nodes. I'm assuming that these are equdistant
# # 	iso.b2 <-lcmm(meas~year,subject='GEOID10',mixture=~year,ng=2,idiag=F,data=iso.b, link="4-quant-splines")
# # 	iso.b3 <-lcmm(meas~year,subject='GEOID10',mixture=~year,ng=3,idiag=T,data=iso.b,link="4-quant-splines")
# # 	iso.b4 <-lcmm(meas~year,subject='GEOID10',mixture=~year,ng=4,idiag=T,data=iso.b,link="4-quant-splines")
# # 	iso.b5 <-lcmm(meas~year,subject='GEOID10',mixture=~year,ng=5,idiag=T,data=iso.b,link="4-quant-splines")
# # 	iso.b6 <-lcmm(meas~year,subject='GEOID10',mixture=~year,ng=6,idiag=T,data=iso.b,link="4-quant-splines")
# # 	# iso.b7 <-lcmm(meas~year,subject='GEOID10',mixture=~year,ng=7,idiag=T,data=iso.b,link="4-quant-splines")
# # 	# iso.b8 <-lcmm(meas~year,subject='GEOID10',mixture=~year,ng=8,idiag=T,data=iso.b,link="4-quant-splines")
# # 	# iso.b9 <-lcmm(meas~year,subject='GEOID10',mixture=~year,ng=9,idiag=T,data=iso.b,link="4-quant-splines")
# # 	# iso.b10 <-lcmm(meas~year,subject='GEOID10',mixture=~year,ng=10,idiag=T,data=iso.b,link="4-quant-splines")
# # 	# iso.b11 <-lcmm(meas~year,subject='GEOID10',mixture=~year,ng=11,idiag=T,data=iso.b,link="4-quant-splines")

# # 	# Choose model using lowest BIC, which is d5
# # 	iso.b2$BIC
# # 	iso.b3$BIC
# # 	iso.b4$BIC
# # 	iso.b5$BIC # Relativly low BIC but chosen for parsimony
# # 	iso.b6$BIC
# # 	iso.b7$BIC
# # 	iso.b8$BIC
# # 	iso.b9$BIC
# # 	iso.b10$BIC
# # 	iso.b11$BIC
# # 	# Of the 8 groupings, class 4 has the lowest BIC and we go with that.

# # 	postprob(iso.b4)
# # 	postprob(iso.b5)
# # 	postprob(iso.b6)
# # 	postprob(iso.b7)
# # # Visualize trajectory
# # 	iso.b3t <- as.data.frame(iso.b3$pprob[,1:2])
# # 	iso.b4t <- as.data.frame(iso.b4$pprob[,1:2])
# # 	iso.b5t <- as.data.frame(iso.b5$pprob[,1:2]) # subset GEOID10 and class name 1:5
# # 	iso.b6t <- as.data.frame(iso.b6$pprob[,1:2])
# # 	iso.b7t <- as.data.frame(iso.b7$pprob[,1:2])
# # 	iso.b$group3 <- factor(iso.b3t$class[sapply(iso.b$GEOID10, function(x) which(iso.b3t$GEOID10==x))])
# # 	iso.b$group4 <- factor(iso.b4t$class[sapply(iso.b$GEOID10, function(x) which(iso.b4t$GEOID10==x))])
# # 	iso.b$group5 <- factor(iso.b5t$class[sapply(iso.b$GEOID10, function(x) which(iso.b5t$GEOID10==x))]) # Add class category to
# # 	iso.b$group6 <- factor(iso.b6t$class[sapply(iso.b$GEOID10, function(x) which(iso.b6t$GEOID10==x))])
# # 	iso.b$group7 <- factor(iso.b7t$class[sapply(iso.b$GEOID10, function(x) which(iso.b7t$GEOID10==x))])


# # 	ggplot(iso.b, aes(year, meas, group=GEOID10, colour=group3)) +
# # 				geom_smooth(aes(group=group3), method="loess", size=2, se=T) +
# # 				labs(x="Year",y="Isolation",colour="Latent Class", title="Black Isolation")
# # 	ggplot(iso.b, aes(year, meas, group=GEOID10, colour=group4)) +
# # 				geom_smooth(aes(group=group4), method="loess", size=2, se=T) +
# # 				labs(x="Year",y="Isolation",colour="Latent Class", title="Black Isolation")
# # iso.b5.plot <- ggplot(iso.b, aes(year, meas, group=GEOID10, colour=group5)) +
# # 				geom_smooth(aes(group=group5), method="loess", size=2, se=T) +
# # 				labs(x="Year",y="Isolation",colour="Latent Class", title="Black Isolation") +
# # 				scale_color_discrete(labels=c("1: N=16 (10.96%)", "2: N=8 (5.48%)", "3: N=42 (28.77%)", "4: N=30 (20.55%)","5: N=50 (34.25%)"))

# # 	ggplot(iso.b, aes(year, meas, group=GEOID10, colour=group6)) +
# # 				geom_smooth(aes(group=group6), method="loess", size=2, se=T) +
# # 				labs(x="Year",y="Isolation",colour="Latent Class", title="Black Isolation")
# # 	ggplot(iso.b, aes(year, meas, group=GEOID10, colour=group7)) +
# # 				geom_smooth(aes(group=group7), method="loess", size=2, se=T) +
# # 				labs(x="Year",y="Isolation",colour="Latent Class", title="Black Isolation")

# # ### Thiel's H - Entropy of multi-group ###
# # # Set up data for analysis to wide then long format
# # 	Hcalc <- sea.f@data %>%
# # 		group_by(GEOID10) %>%
# # 		select(Hcalc1980,Hcalc1990,Hcalc2000,Hcalc2010) %>%
# # 		gather(year, meas, -GEOID10) %>% 	# reshape wide to long
# # 		separate(year,into=c("int","year"),sep=5) %>%
# # 		arrange(GEOID10)

# # 		glimpse(Hcalc)

# # 	# do the LCMM calculations for classes 2:9 for changes in disadvantage. These segregation models require link spline, where there are 4 nodes. I'm assuming that these are equdistant
# # 	# Hcalc2 <-lcmm(meas~year,subject='GEOID10',mixture=~year,ng=2,idiag=F,data=Hcalc, link="beta")
# # 	# Hcalc3 <-lcmm(meas~year,subject='GEOID10',mixture=~year,ng=3,idiag=T,data=Hcalc,link="beta")
# # 	Hcalc4 <-lcmm(meas~year,subject='GEOID10',mixture=~year,ng=4,idiag=T,data=Hcalc,link="beta")
# # 	Hcalc5 <-lcmm(meas~year,subject='GEOID10',mixture=~year,ng=5,idiag=T,data=Hcalc,link="beta")
# # 	Hcalc6 <-lcmm(meas~year,subject='GEOID10',mixture=~year,ng=6,idiag=T,data=Hcalc,link="beta")
# # 	# Hcalc7 <-lcmm(meas~year,subject='GEOID10',mixture=~year,ng=7,idiag=T,data=Hcalc,link="beta")
# # 	# Hcalc8 <-lcmm(meas~year,subject='GEOID10',mixture=~year,ng=8,idiag=T,data=Hcalc,link="beta")
# # 	# Hcalc9 <-lcmm(meas~year,subject='GEOID10',mixture=~year,ng=9,idiag=T,data=Hcalc,link="beta")
# # 	# Hcalc10 <-lcmm(meas~year,subject='GEOID10',mixture=~year,ng=10,idiag=T,data=Hcalc,link="beta")
# # 	# Hcalc11 <-lcmm(meas~year,subject='GEOID10',mixture=~year,ng=11,idiag=T,data=Hcalc,link="beta")

# # 	# Choose model using lowest BIC, which is d5
# # 	Hcalc2$BIC
# # 	Hcalc3$BIC
# # 	Hcalc4$BIC
# # 	Hcalc5$BIC # Relativly low BIC but chosen for parsimony
# # 	Hcalc6$BIC
# # 	Hcalc7$BIC
# # 	Hcalc8$BIC
# # 	Hcalc9$BIC
# # 	Hcalc10$BIC
# # 	Hcalc11$BIC
# # 	# Of the 8 groupings, class 4 has the lowest BIC and we go with that.

# # 	postprob(Hcalc4)
# # 	postprob(Hcalc5)
# # 	postprob(Hcalc6)
# # 	postprob(Hcalc7)
# # 	postprob(Hcalc10)
# # # Visualize trajectory
# # 	Hcalc2t <- as.data.frame(Hcalc2$pprob[,1:2])
# # 	Hcalc3t <- as.data.frame(Hcalc3$pprob[,1:2])
# # 	Hcalc4t <- as.data.frame(Hcalc4$pprob[,1:2])
# # 	Hcalc5t <- as.data.frame(Hcalc5$pprob[,1:2]) # subset GEOID10 and class name 1:5
# # 	Hcalc6t <- as.data.frame(Hcalc6$pprob[,1:2])
# # 	Hcalc7t <- as.data.frame(Hcalc7$pprob[,1:2])
# # 	Hcalc8t <- as.data.frame(Hcalc8$pprob[,1:2])
# # 	Hcalc9t <- as.data.frame(Hcalc9$pprob[,1:2])
# # 	Hcalc10t <- as.data.frame(Hcalc10$pprob[,1:2])
# # 	Hcalc11t <- as.data.frame(Hcalc11$pprob[,1:2])
# # 	# Add group categories to main data
# # 	Hcalc$group2 <- factor(Hcalc2t$class[sapply(Hcalc$GEOID10, function(x) which(Hcalc2t$GEOID10==x))])
# # 	Hcalc$group3 <- factor(Hcalc3t$class[sapply(Hcalc$GEOID10, function(x) which(Hcalc3t$GEOID10==x))])
# # 	Hcalc$group4 <- factor(Hcalc4t$class[sapply(Hcalc$GEOID10, function(x) which(Hcalc4t$GEOID10==x))])
# # 	Hcalc$group5 <- factor(Hcalc5t$class[sapply(Hcalc$GEOID10, function(x) which(Hcalc5t$GEOID10==x))]) # Add class category to
# # 	Hcalc$group6 <- factor(Hcalc6t$class[sapply(Hcalc$GEOID10, function(x) which(Hcalc6t$GEOID10==x))])
# # 	Hcalc$group7 <- factor(Hcalc7t$class[sapply(Hcalc$GEOID10, function(x) which(Hcalc7t$GEOID10==x))])
# # 	Hcalc$group8 <- factor(Hcalc8t$class[sapply(Hcalc$GEOID10, function(x) which(Hcalc8t$GEOID10==x))])
# # 	Hcalc$group9 <- factor(Hcalc9t$class[sapply(Hcalc$GEOID10, function(x) which(Hcalc9t$GEOID10==x))])
# # 	Hcalc$group10 <- factor(Hcalc10t$class[sapply(Hcalc$GEOID10, function(x) which(Hcalc10t$GEOID10==x))])
# # 	Hcalc$group11 <- factor(Hcalc11t$class[sapply(Hcalc$GEOID10, function(x) which(Hcalc11t$GEOID10==x))])

# # 	# ggplot(Hcalc, aes(year, meas, group=GEOID10, colour=group2)) +
# # 	# 			geom_smooth(aes(group=group2), method="loess", size=2, se=T) +
# # 	# 			labs(x="Year",y="Isolation",colour="Latent Class", title="Thiel's H - Entropy")
# # 	# ggplot(Hcalc, aes(year, meas, group=GEOID10, colour=group3)) +
# # 	# 			geom_smooth(aes(group=group3), method="loess", size=2, se=T) +
# # 	# 			labs(x="Year",y="Isolation",colour="Latent Class", title="Thiel's H - Entropy")
# # 	# ggplot(Hcalc, aes(year, meas, group=GEOID10, colour=group4)) +
# # 	# 			geom_smooth(aes(group=group4), method="loess", size=2, se=T) +
# # 	# 			labs(x="Year",y="Isolation",colour="Latent Class", title="Thiel's H - Entropy")
# # 	Hcalc5.plot <- ggplot(Hcalc, aes(year, meas, group=GEOID10, colour=group5)) +
# # 				geom_smooth(aes(group=group5), method="loess", size=2, se=T) +
# # 				labs(x="Year",y="Isolation",colour="Latent Class", title="Thiel's H - Entropy") +
# # 				scale_color_discrete(labels=c("1: N=10 (6.85%)", "2: N=26 (17.81%)", "3: N=57 (39.04%)", "4: N=30 (20.55%)","5: N=23 (15.75%)"))

# # 	# ggplot(Hcalc, aes(year, meas, group=GEOID10, colour=group6)) +
# # 	# 			geom_smooth(aes(group=group6), method="loess", size=2, se=T) +
# # 	# 			labs(x="Year",y="Isolation",colour="Latent Class", title="Thiel's H - Entropy")
# # 	# ggplot(Hcalc, aes(year, meas, group=GEOID10, colour=group7)) +
# # 	# 			geom_smooth(aes(group=group7), method="loess", size=2, se=T) +
# # 	# 			labs(x="Year",y="Isolation",colour="Latent Class", title="Thiel's H - Entropy")
# # 	# ggplot(Hcalc, aes(year, meas, group=GEOID10, colour=group8)) +
# # 	# 			geom_smooth(aes(group=group8), method="loess", size=2, se=T) +
# # 	# 			labs(x="Year",y="Isolation",colour="Latent Class", title="Thiel's H - Entropy")
# # 	# ggplot(Hcalc, aes(year, meas, group=GEOID10, colour=group9)) +
# # 	# 			geom_smooth(aes(group=group9), method="loess", size=2, se=T) +
# # 	# 			labs(x="Year",y="Isolation",colour="Latent Class", title="Thiel's H - Entropy")
# # 	# ggplot(Hcalc, aes(year, meas, group=GEOID10, colour=group10)) +
# # 	# 			geom_smooth(aes(group=group10), method="loess", size=2, se=T) +
# # 	# 			labs(x="Year",y="Isolation",colour="Latent Class", title="Thiel's H - Entropy")
# # 	# ggplot(Hcalc, aes(year, meas, group=GEOID10, colour=group11)) +
# # 	# 			geom_smooth(aes(group=group11), method="loess", size=2, se=T) +
# # 	# 			labs(x="Year",y="Isolation",colour="Latent Class", title="Thiel's H - Entropy")

# # # Final plots of LCMM
# # 	dis5.plot # Disadvantage
# # 	d.wb5.plot # Dissimilarity
# # 	int.wb6.plot # Interaction
# # 	iso.b5.plot # Black Isolaction
# # 	Hcalc5.plot # Entropy




# # 	data <- merge(dis5t,d.wb5t,by="GEOID10")
# # 	data <- merge(data, int.wb6t, by="GEOID10")
# # 	colnames(data) <- c("GEOID10","Disadvantage","Dissimilarity","Interaction")
# # 	data <- merge(data, iso.b5t, by="GEOID10")
# # 	data <- merge(data,Hcalc5t, by="GEOID10")
# # 	head(data)
# # 	colnames(data) <- c("GEOID10","Disadvantage","Dissimilarity","Interaction","Isolation","Entropy")
# # 	head(data)

# # 	sea.f@data <- merge(sea.f@data, data, by="GEOID10")
# # 	# write.csv(data,"/Users/timothythomas/Google Drive/UW/Research/SCP/Data/Tables/Edits/Data_LCMMCats_151229.csv")

# # load("/Users/timothythomas/Google Drive/UW/Research/SCP/Data/RData/SCP_data_for_analysis_122715.RData")
# # # load("/Users/timothythomas/Documents/Data/SPD2008-2012.RData")

# # ###########################
# # ### Merge Chuck Dataset ###
# # ###########################

# # # Map points
# # 	library(GISTools)
# # 	df.SCP <- as.data.frame(df.SCP.analysis)

# # 	# Remove NA's from coordinates
# # 		df.SCP <- na.omit(df.SCP)

# # 	# Convert data to spatial dataframe
# # 		coordinates(df.SCP) <- c("MIDPT_X","MIDPT_Y")

# # 	# Convert df to sea shapefile
# # 		proj4string(df.SCP) <- proj4string(sea)

# # 	# Identify points in polygons
# # 		spdf.SCP <- cbind(df.SCP, over(df.SCP,sea.f))

# # 		saveRDS(spdf.SCP, "../Data/RData/SCP_SpatialLCMM_151229.rds")



# # # 		sea.f <- sea
# # # 		df.SCP$PRIMARY_KEY <- poly.counts(sea.f, df.SCP) # (points,polygons)
# # # 		over(pts,as(ply,"SpatialPolygons"))

# # # 		test <- as(sea,"SpatialPolygons")
# # # 	###########
# # # 		select.pts <- df.SCP[!is.na(over(df.SCP,as(sea,"SpatialPolygons"))),]
# # # 		head(select.pts@data)

# # # #########
# # # 	### GIS operations in R ###
# # # 		require(GISTools)
# # # 	### Convert to spatial dataframe ###
# # # 		# Turn to spatial data frame
# # # 		coordinates(data) <- c("x","y")

# # # plot(sea)
# # # plot(df.SCP, add=T)

# # # df.SCP[,c("MIDPT_X","MIDPT_Y")]


# # # proj4string(sea)
# # # proj4string(df.SCP) <- proj4string(sea)


# # # #########################
# # # # Map Trajectory groups #
# # # #########################

# # # # Map disadvnatage trajectories
# # # 	# Set up data

# # # sea.ff <- fortify(sea.f, region="GEOID10")
# # # sea.ff <- merge(sea.ff,Hcalc, by.x="GEOID10", by.y="id")

# # # 	dis5$GEOID10 <- as.character(dis5$GEOID10)
# # # 	colnames(dis5)[2] <- "disad.class"
# # # 	sea.ff@data <- merge(sea.f@data, dis5[,c(""), by.x="GEOID10", by.y="GEOID10")
# # # 	glimpse(sea.f)




# # # # 	# plot map

# # # # 	sea.ff <- fortify(sea.f, region="GEOID10")

# # # # qplot(data=sea.ff, x=long, y=lat, fill="disad.class",group="GEOID10",geom="polygon")


# # # # ggplot(sea.ff, aes(long, lat, group=group, fill="disad.class")) +
# # # # 			geom_polygon()
# # # # 				geom_smooth(aes(group=group3), method="loess", size=2, se=T) +
# # # # 				labs(x="Year",y="Proportion",colour="Latent Class")



# # # # # Set up data for analysis
# # # # 	dis <- sea.f@data %>%
# # # # 		group_by(GEOID10) %>%
# # # # 		select(dis1980,dis1990,dis2000,dis2010) %>%
# # # # 		gather(year, meas, -GEOID10) %>% 	# reshape wide to long
# # # # 		separate(year,into=c("dis","year"),sep=3) %>%
# # # # 		arrange(GEOID10)

# # # # 	# race change in
# # # # 	d2 <-lcmm(meas~year,subject='GEOID10',mixture=~year,ng=2,idiag=T,data=dis,link="linear")
# # # # 	d3 <-lcmm(meas~year,subject='GEOID10',mixture=~year,ng=3,idiag=T,data=dis,link="linear")
# # # # 	d4 <-lcmm(meas~year,subject='GEOID10',mixture=~year,ng=4,idiag=T,data=dis,link="linear")
# # # # 	d5 <-lcmm(meas~year,subject='GEOID10',mixture=~year,ng=5,idiag=T,data=dis,link="linear")
# # # # 	d6 <-lcmm(meas~year,subject='GEOID10',mixture=~year,ng=6,idiag=T,data=dis,link="linear")
# # # # 	d7 <-lcmm(meas~year,subject='GEOID10',mixture=~year,ng=7,idiag=T,data=dis,link="linear")
# # # # 	d8 <-lcmm(meas~year,subject='GEOID10',mixture=~year,ng=8,idiag=T,data=dis,link="linear")
# # # # 	d9 <-lcmm(meas~year,subject='GEOID10',mixture=~year,ng=9,idiag=T,data=dis,link="linear")

# # # # 	# Choose model using lowest BIC, which is d5
# # # # 	d2$BIC
# # # # 	d3$BIC
# # # # 	d4$BIC
# # # # 	d5$BIC
# # # # 	d6$BIC
# # # # 	d7$BIC
# # # # 	d8$BIC
# # # # 	d9$BIC

# # # # 	summary(d2) #
# # # # 	postprob(d2)
# # # # 	summary(d3) #
# # # # 	postprob(d3)
# # # # 	summary(d4) #
# # # # 	postprob(d4)
# # # # 	summary(d5) # Lowest BIC
# # # # 	postprob(d5)
# # # # 	summary(d6) #
# # # # 	postprob(d6)
# # # # # Visualize trajectory
# # # # 	people5 <- as.data.frame(d5$pprob[,1:2])
# # # # 	dis$group5 <- factor(people5$class[sapply(dis$GEOID10, function(x) which(people5$GEOID10==x))])

# # # # 	ggplot(dis, aes(year, meas, group=GEOID10, colour=group5)) +
# # # # 				geom_smooth(aes(group=group5), method="loess", size=2, se=T) +
# # # # 				labs(x="Year",y="Disadvantage Index",colour="Latent Class", title="LCMM of Disadvantage")

# # # # 	# map



# # # # # t.sea <- sea

# # # # # # City level totals for the total population and each race group
# # # # # sea_tot <- tapply(t.sea$tot2010, t.sea$GEOID10, sum)
# # # # # sea_tot <- data.frame(tracts=names(unlist(sea_tot)),pop=unlist(sea_tot))

# # # # # 102 + 325 + 145 + 3538
# # # # # 500+810+705+3400


# # # # # # test.sea <- sea
# # # # # # test.sea@data$his2010 <- .5

# # # # # # test1 <- spseg(sea,data=sea@data[,32:35], smoothing="equal")
# # # # # # test2 <- spseg(sea,data=sea@data[,32:35])

# # # # # # as(test1, "SpatialPointsDataFrame")[["his2010"]]
# # # # # # as(test2, "SpatialPointsDataFrame")[["his2010"]]

# # # # # # as(as(test,"SpatialPointsDataFrame"),"data.frame")




# # # # # #################################################



# # # # # 	### Segregation Measures
# # # # # 		seg <- sea
# # # # # 		# Manual segregation measure
# # # # # 			# Create city totals for all white
# # # # # 			seg$city_tot10 <- sum(seg$tot2010)
# # # # # 			seg$city_wht10 <- sum(seg$wht2010*seg$tot2010)
# # # # # 			seg$city_blk10 <- sum(seg$blk2010*seg$tot2010)
# # # # # 			seg$city_asi10 <- sum(seg$asi2010*seg$tot2010)
# # # # # 			seg$city_his10 <- sum(seg$his2010*seg$tot2010)

# # # # # 			# City wide race proportions
# # # # # 			seg$city_pwht10 <- seg$city_wht10/seg$city_tot10
# # # # # 			seg$city_pblk10 <- seg$city_blk10/seg$city_tot10
# # # # # 			seg$city_pasi10 <- seg$city_asi10/seg$city_tot10
# # # # # 			seg$city_phis10 <- seg$city_his10/seg$city_tot10
# # # # # 			glimpse(seg)

# # # # # 			# City level entropy
# # # # # 			seg$city_Ent <- seg$city_pwht10*(log(1/seg$city_pwht10)) +
# # # # # 							seg$city_pblk10*(log(1/seg$city_pblk10)) +
# # # # # 							seg$city_pasi10*(log(1/seg$city_pasi10)) +
# # # # # 							seg$city_phis10*(log(1/seg$city_phis10))

# # # # # 			seg$city_Ent <- ifelse(is.na(seg$city_Ent)==T,0,seg$city_Ent)
# # # # # 			summary(seg$city_Ent)

# # # # # 			# Tract level entropy measure
# # # # # 			seg$tr_ent <- seg$wht2010*(log(1/seg$wht2010)) +
# # # # # 							seg$blk2010*(log(1/seg$blk2010)) +
# # # # # 							seg$asi2010*(log(1/seg$asi2010)) +
# # # # # 							seg$his2010*(log(1/seg$his2010))

# # # # # 			seg$tr_ent<- ifelse(is.na(seg$tr_ent)==T, 0,seg$tr_ent)

# # # # # 			# Tract's contribution to the H index
# # # # # 			seg$Hcalc <- (seg$tot2010*(seg$city_Ent-seg$tr_ent))/(seg$city_tot10*seg$city_Ent)
# # # # # 			seg$Hcalc <- ifelse(is.na(seg$Hcalc)==T,0,seg$Hcalc)
# # # # # 			seg$hindex <- unlist(tapply(seg$Hcalc,seg$GEOID10,sum))

# # # # # 			# Plot histogram
# # # # # 			par(mfrow=c(2,2))
# # # # # 			hist(seg$hindex, main="WHite-Black-Asian-Hispanic Theile H Index")

# # # # # 			# Plot Map

# # # # # 			brks <- quantile(seg$hindex, probs=c(0,.25, .5, .75, 1))
# # # # # 			cols <- brewer.pal(n=5,name="Blues")
# # # # # 			spplot(seg,zcol="hindex",at=brks,col.regions=cols,main="Four-Race Theilr H Index, 2010")
# # # # # 			spplot(seg,zcol="hindex",identify=TRUE)
# # # # # 			spplot(seg, zcol="dis10", main="Disadvantage Index")



# # # # # #########
# # # # # # Plots #
# # # # # #########

# # # # # 	# Choropleth map of white residents
# # # # # 		# plot white (https://github.com/hadley/ggplot2/wiki/plotting-polygon-shapefiles)
# # # # # 			sea@data$id = rownames(sea@data)
# # # # # 			seaf <- fortify(sea, region="id")
# # # # # 			sea.df <- merge(seaf, sea@data, by="id")

# # # # # 			ggplot(sea.df) +
# # # # # 				aes(long,lat,group=group,fill=wht2010) +
# # # # # 				geom_polygon() +
# # # # # 				geom_path(color="white") +
# # # # # 				coord_equal() +
# # # # # 				ggtitle("Proportion White")


# # # # # 			ggplot(sea.df) +
# # # # # 				aes(long,lat,group=group,fill=asi2010) +
# # # # # 				geom_polygon() +
# # # # # 				geom_path(color="white") +
# # # # # 				coord_equal()

# # # # # 	# trajectory of tracts for disadvantage
# # # # # 		mdf <- melt(dis_index, id.vars="GEOID10",value.name="value",variable.name="Year")

# # # # # 		ggplot(mdf, aes(x=Year, y=value, group=GEOID10, colour=GEOID10)) +
# # # # # 			geom_line() +
# # # # # 			geom_smooth()
# # # # # 			geom_point(size=4, shape=21,fill="white")
# # # # # 			ggtitle("All Tracts in Seattle")


# # # # # ####################
# # # # # # Spatial Analysis #
# # # # # ####################

# # # # # #####################
# # # # # # Test - DO NOT RUN #
# # # # # #####################
# # # # # 			city <- sea
# # # # # 		### Block level entropy
# # # # # 			city@data$entity <- city@data$wht2010*(log(1/city@data$wht2010)) +
# # # # # 								city@data$blk2010*(log(1/city@data$blk2010)) +
# # # # # 								city@data$asi2010*(log(1/city@data$asi2010)) +
# # # # # 								city@data$his2010*(log(1/city@data$his2010))
# # # # # 			city@data$entity<- ifelse(is.na(city@data$entity)==T, 0,city@data$entity)

# # # # # 		### H index
# # # # # 			city@data$Hcalc <- (city@data$tot2010*(city@data$city_Ent-city@data$entity))/(city@data$city_Total*city@data$city_Ent)
# # # # # 			city@data$Hcalc <- ifelse(is.na(city@data$Hcalc)==T,0,city@data$Hcalc)
# # # # # 			hindex <- unlist(tapply(city@data$Hcalc,city@data$GISJOIN,sum))

# # # # # 		### Assemble indeces into dataframe
# # # # # 			city_Dat <- data.frame(GISJOIN=names(unlist(dissim.wb.bg)),dissim_wb=unlist(dissim.wb.bg),isolation_b=isol.b.bg,interaction_bw=int.wb.bg,TheileH=hindex)

# # # # # 		### Plot Histograms
# # # # # 			par(mfrow=c(2,2))
# # # # # 			hist(city_Dat$dissim_wb,main="White-Black Dissimilairy")
# # # # # 			hist(city_Dat$isolation_b,main="Black Isolation")
# # # # # 			hist(city_Dat$interaction_bw,main="White-Black Interaction")
# # # # # 			hist(city_Dat$TheileH, main="White-Black-Asian Theile H Index")

# # # # # 		### Join data to shapefile
# # # # # 			city@data <- data.frame(city@data,city_Dat[match(city@data$GISJOIN,Sea_Dat$GISJOIN),])


# # # # # ## End(Not run)

# # # # # # ##############################################################
# # # # # # ##########################Excess Data#########################
# # # # # # ##############################################################
# # # # # # ### Indexing ###
# # # # # # 		# 2010
# # # # # # 		subsea$zwelf <- scale(subsea$welf)
# # # # # # 		subsea$zpov <- scale(subsea$pov)
# # # # # # 		subsea$zunemp <- scale(subsea$unemp)
# # # # # # 		subsea$zfemh <- scale(subsea$femh)
# # # # # # 		subsea$zchild <- scale(subsea$child)

# # # # # # 		subsea_dis <- subsea %>% group_by(GEOID10 = GEO2010) %>% summarise(disix=sum(zwelf, zpov, zunemp, zfemh, zchild))
# # # # # # 		glimpse(subseadis)

# # # # # # 		sea@data <- left_join(sea@data,subsea_dis, by.x="GEOID10", by.y="GEO2010")


# # # # # # 		scale(subsea$welf)
# # # # # # 		subsea$zpov <- scale(subsea$pov)
# # # # # # 		subsea$zunemp <- scale(subsea$unemp)
# # # # # # 		subsea$zfemh <- scale(subsea$femh)
# # # # # # 		subsea$zchild <- scale(subsea$child)
# # # # # # 		glimpse(subsea)
# # # # # # 		subsea$disix <- rowsum(as.numeric(subsea[,c("zwelf", "zpov", "zunemp", "zfemh", "zchild")]))


# # # # # # 		subsea$disindex <- rowSums(subsea[,7:11])


# # # # # # 		disindex <- subsea %>%
# # # # # # 					mutate_each(as.numeric(scale()))
# # # # # # 					 %>%

# # # # # # 		dindex <- subsea %>%
# # # # # # 			mutate(subsea,zWELF = as.numeric(scale(welf)))
# # # # # # 				,
# # # # # # 					zPOV = as.numeric(scale(pov)),
# # # # # # 					zUNEMP = as.numeric(scale(unemp)),
# # # # # # 					zFEMH = as.numeric(scale(femh)),
# # # # # # 					zCHILD = as.numeric(scale(child)))




# # # # # # 			mutate(subsea, zWELF = scale(welf),
# # # # # # 					zPOV = scale(pov),
# # # # # # 					zUNEMP = scale(unemp),
# # # # # # 					zFEMH = scale(femh),
# # # # # # 					zCHILD = scale(child))
# # # # # # 					 %>%
# # # # # # 			mutate(dINDEX = sum(c("zWELF","zPOV","zUNEMP","zFEMH","zCHILD")))

# # # # # # 				zNOHS = (pNOHS - mean(pNOHS, na.rm=T))/sd(pNOHS, na.rm=T),
# # # # # # 				zSIX5 = (pSIX5 - mean(pSIX5, na.rm=T))/sd(pSIX5, na.rm=T),
# # # # # # 				zNONH = (pNONH - mean(pNONH, na.rm=T))/sd(pNONH, na.rm=T),
# # # # # # 				zWHITE = (pWHITE - mean(pWHITE, na.rm=T))/sd(pWHITE, na.rm=T),
# # # # # # 				zBLACK = (pBLACK - mean(pBLACK, na.rm=T))/sd(pBLACK, na.rm=T),
# # # # # # 				zASIAN = (pASIAN - mean(pASIAN, na.rm=T))/sd(pASIAN, na.rm=T),
# # # # # # 				zOTHER = (pOTHER - mean(pOTHER, na.rm=T))/sd(pOTHER, na.rm=T),
# # # # # # 				zHISP = (pHISP - mean(pHISP, na.rm=T))/sd(pHISP, na.rm=T))
# # # # # # 		clet@data$SESix <- rowSums(clet@data[,c("zPOV", "zWELF", "zUNEMP", "zFEMH", "zNOHS")])



# # # # # # 	### Principal Component Analysis ###
# # # # # # 		PCAdis <- prcomp(subsea[,2:6], center = TRUE, scale. = TRUE)

# # # # # # 		print(PCAdis)
# # # # # # 		plot(PCAdis, type = "l")
# # # # # # 		summary(PCAdis)

# # # # # # 	# Spatial Disadvantage
# # # # # # 		# Spatial Disadvantage is measured by taking the average value on the
# # # # # # 		# measure of concentrated disadvantage in all tracts that surround the
# # # # # # 		# focal tract using the “queen criterion,” which means that all tracts
# # # # # # 		# sharing a border or a vertex with the focal tract are given equal weight
# # # # # # 		# in the calculation of spatial disadvantage. Tracts that do not share a
# # # # # # 		# border or vertex with the focal tract are assigned a weight of zero and
# # # # # # 		# thus are not included in the calculation of spatial disadvantage.


# # # # # # 	subsea_scale <- subsea %>%
# # # # # # 					mutate_each(as.numeric(scale(.)))



# # # # # # 	sea.seg <- spseg(x=sea,data=sea@data[,32:35])
# # # # # # 	sea.seg # segregation values
# # # # # # 	spplot(as(sea.seg, "SpatialPointsDataFrame")["his2010"])
# # # # # # 	spplot(sea.seg)

# # # # # # 	test<- sea
# # # # # # 	test@data$wht2010 <- .5
# # # # # # 	glimpse(test@data)
# # # # # # 	test.seg <- spseg(x=test, data=test@data[,32:35])
# # # # # # 	test.seg
# # # # # # 	spplot(test.seg)

# # # # # # 	# as a visual check, I'll change all the wht2010 proportions to .5
# # # # # # 	sea@data$wht2010 <- .5
# # # # # # 	sea.seg <- spseg(x=sea, data=sea@data[,32:35])
# # # # # # 	spplot(sea.seg)


# # # # # # 	seattle <- sea
# # # # # # 	seattle@data <- seattle@data[,c("GEOID10","wht2010","blk2010","asi2010","his2010")]

# # # # # # 	writeOGR(seattle,"/Users/timothythomas/Dropbox/Public","seattle", driver="ESRI Shapefile")

# # # # # # 	install.packages("seg")
# # # # # # 	install.packages("rgdal")
# # # # # # 	library(seg)
# # # # # # 	library(rgdal)

# # # # # # 	# load data
# # # # # # 	sea.shp <- readOGR("/Users/timothythomas/Dropbox/Public/seattle","seattle")

# # # # # # 	# run segregation measure
# # # # # # 	sea.seg <- spseg(x=sea.shp, data=sea.shp@data[,2:5])
# # # # # # 	print(sea.seg, digits=3)
# # # # # # 	as.list(sea.seg)
# # # # # # 	spplot(sea.seg)



# # # # # # 	### Segregation measure
# # # # # # 		a <- spseg(sea, sea@data[,c("wht2010","blk2010","asi2010","his2010")])
# # # # # # 		print(a, digit=3)
# # # # # # 		print(spplot(a, main="No Smoothing"))
# # # # # # 		# smooth
# # # # # # 		b <- spseg(sea, sea@data[,c("wht2010","blk2010","asi2010","his2010")], smoothing="equal")
# # # # # # 		spplot(b,main="Equal")

# # # # # # 		w <- matrix(c(1.5, 1.5,
# # # # # #               1.5, 8.5,
# # # # # #               8.5, 8.5,
# # # # # #               8.5, 4.5,
# # # # # #               5.5, 4.5,
# # # # # #               5.5, 1.5), ncol = 2, byrow = TRUE)
# # # # # # 		c <- spseg(sea,
# # # # # # 					sea@data[,c("wht2010","blk2010","asi2010","his2010")],
# # # # # # 					smoothing="kernel",
# # # # # # 					window=w,
# # # # # # 					nrow=200,
# # # # # # 					ncol=200)
# # # # # # 		spplot(c, main="Kernel")
# # # # # # 		as.list(a)
# # # # # # 		spplot(as(c, "SegLocal"), main = "Local population composition")

# # # # # # 	sea@data$id = rownames(sea@data)
# # # # # # 	seaf <- fortify(sea, region="id")
# # # # # # 	sea.df <- merge(seaf, sea@data, by="id")


# # # # # # 	# plot white (https://github.com/hadley/ggplot2/wiki/plotting-polygon-shapefiles)
# # # # # # 	ggplot(sea.df) +
# # # # # # 		aes(long,lat,group=group,fill=wht2010) +
# # # # # # 		geom_polygon() +
# # # # # # 		geom_path(color="white") +
# # # # # # 		coord_equal()

# # # # # # 	ggplot(sea.df) +
# # # # # # 		aes(long,lat,group=group,fill=asi2010) +
# # # # # # 		geom_polygon() +
# # # # # # 		geom_path(color="white") +
# # # # # # 		coord_equal()

# # # # # # # Get individual values from spseg output
# # # # # # # uses the idealised landscapes in 'segdata'
# # # # # # data(segdata)

# # # # # # grd <- GridTopology(cellcentre.offset=c(0.5,0.5),
# # # # # # cellsize=c(1,1), cells.dim=c(10,10))
# # # # # # grd.sp <- as.SpatialPolygons.GridTopology(grd)
# # # # # # test.df <- segdata[,1:2]
# # # # # # # no spatial smoothing
# # # # # # xx1 <- spseg(grd.sp, data = test.df)
# # # # # # print(xx1, digits = 3)
# # # # # # # plots the values in the slot 'data'
# # # # # # spplot(xx1, main = "No spatial smoothing")
# # # # # # # smoothes the data points
# # # # # # xx2 <- spseg(grd.sp, data = test.df, smoothing = "equal")
# # # # # # print(xx2, digits = 3)
# # # # # # spplot(xx2, main = "Equal")
# # # # # # # uses the kernel smoothing of the data points
# # # # # # xx3 <- spseg(grd.sp, data = test.df, smoothing = "kernel",
# # # # # # nrow = 20, ncol = 20)
# # # # # # print(xx3, digits = 3)
# # # # # # spplot(xx3, main = "Kernel")
# # # # # # ## Not run:
# # # # # # # same as the above but with a boundary polygon
# # # # # # w <- matrix(c(1.5, 1.5,
# # # # # # 1.5, 8.5,
# # # # # # 8.5, 8.5,
# # # # # # 8.5, 4.5,
# # # # # # 5.5, 4.5,
# # # # # # 5.5, 1.5), ncol = 2, byrow = TRUE)
# # # # # # xx4 <- spseg(grd.sp, data = segdata[,1:2], smoothing = "kernel",
# # # # # # window = w, nrow = 20, ncol = 20)
# # # # # # print(xx4, digits = 3)
# # # # # # spplot(xx4, main = "Kernel with a boundary polygon")
# # # # # # # retrieves the index values
# # # # # # as.list(xx4)
# # # # # # # shows the values in the slot 'env'
# # # # # # spplot(as(xx4, "SegLocal"), main = "Local population composition")
# # # # # # ## End(Not run)

# # # # # # seadf <- as.data.frame(sea)
# # # # # # as.list(a)
# # # # # # spplot(a,col.regions=hear.colors(20))
# # # # # # w <- as(a,"SegLocal")
# # # # # # spplot(w)
# # # # # # as(w, "vector")

# # # # # # # creates 100 regularly-spaced data points and 3 population groups
# # # # # # xy <- expand.grid(1:10, 1:10)
# # # # # # colnames(xy) <- c("x", "y")
# # # # # # pop <- matrix(runif(300), ncol = 3)
# # # # # # env <- matrix(runif(300), ncol = 3)
# # # # # # colnames(pop) <- LETTERS[1:3]
# # # # # # colnames(env) <- LETTERS[4:6]

# # # # # # # constructs an object of class 'SegSpatial'
# # # # # # v <- SegSpatial(d = numeric(), r = numeric(), h = numeric(),
# # # # # #                 p = matrix(0, 0, 0),
# # # # # #                 coords = as.matrix(xy), data = pop, env = env)
# # # # # # print(v)

# # # # # # # changes the spatial dissimilarity index value
# # # # # # slot(v, "d") <- runif(1)

# # # # # # # retrieves the index values
# # # # # # as.list(v)

# # # # # # # displays the values in the slot 'data'
# # # # # # spplot(v, col.regions = heat.colors(20))

# # # # # # # displays the values in the slot 'env'
# # # # # # w <- as(v, "SegLocal")
# # # # # # spplot(w, col.regions = heat.colors(20))


# # # # # # extract(a, sea, fun=function(x, ...) length(na.omit(x))/length))

# # # # # # # uses the idealised landscapes in 'segdata'
# # # # # # data(segdata)
# # # # # # grd <- GridTopology(cellcentre.offset=c(0.5,0.5),
# # # # # # cellsize=c(1,1), cells.dim=c(10,10))
# # # # # # grd.sp <- as.SpatialPolygons.GridTopology(grd)
# # # # # # # displays the test data
# # # # # # plot(grd.sp)
# # # # # # plot(grd.sp[segdata[,9] == 100,], col = "Black", add = TRUE)
# # # # # # plot(grd.sp[segdata[,9] == 50,], col = "Grey", add = TRUE)
# # # # # # # tries different bandwidths for the same data
# # # # # # bw1 <- deseg(grd.sp, segdata[,9:10], sigma = 1, nrow = 20, ncol = 20)
# # # # # # print(bw1, digits = 3)
# # # # # # bw1.val <- sum(as.vector(bw1))
# # # # # # spplot(bw1, col.regions=rev(heat.colors(20)),
# # # # # # main = paste("Bandwidth = 1, S =", round(bw1.val, 2)))
# # # # # # bw2 <- deseg(grd.sp, segdata[,9:10], sigma = 2, nrow = 20, ncol = 20)
# # # # # # print(bw2, digits = 3)
# # # # # # bw2.val <- sum(as(bw2, "vector"))
# # # # # # spplot(bw2, col.regions=rev(heat.colors(20)),
# # # # # # main = paste("Bandwidth = 2, S =", round(bw2.val, 2)))
# # # # # # ## Not run:
# # # # # # # let's see how the index value changes with different bandwidths
# # # # # # h0 <- seq(1, 5, length.out = 10); vals <- numeric()
# # # # # # for (i in 1:10) {
# # # # # # d <- deseg(grd.sp, segdata[,9:10], sigma = h0[i], verbose = TRUE)
# # # # # # vals <- append(vals, sum(as(d, "vector")))
# # # # # # }
# # # # # # plot(h0, vals, type = "b", xlab = "Bandwidth", ylab = "S")
# # # # # # title(main = "segdata[,9:10]")

# # # # # # # calculates the index for all data sets in 'segdata'
# # # # # # d.segdata <- matrix(NA, nrow = 3, ncol = 8)
# # # # # # for (i in 1:8) {
# # # # # # idx <- 2 * i
# # # # # # tmp <- deseg(grd.sp, segdata[,(idx-1):idx])
# # # # # # d.segdata[,i] <- as(tmp, "vector")
# # # # # # }
# # # # # # # presents the results as a bar chart
# # # # # # barplot(d.segdata, names.arg = LETTERS[1:8], main = "segdata",
# # # # # # legend.text = c(expression(italic(paste("S"[L]))),
# # # # # # expression(italic(paste("S"[C]))),
# # # # # # expression(italic(paste("S"[Q])))),
# # # # # # args.legend = list(x = "topright", horiz = TRUE))
# # # # # 		# Dplyr test
# # # # # # 		sea.results <-
# # # # # # 					sea@data %>%
# # # # # # 					group_by(GEOID10) %>%
# # # # # # 					summarise(seg80=spseg(sea,data=slot(sea,"data")[,c("wht1980","blk1980","asi1980","his1980")]))

# # # # # # 					test <- sea;glimpse(test@data)

# # # # # # 		# Normal loop

# # # # # # 		Dis80 <- NA
# # # # # # 		Dis90 <- NA
# # # # # # 		Dis00 <- NA
# # # # # # 		Dis10 <- NA
# # # # # # 		H80 <- NA
# # # # # # 		H90 <- NA
# # # # # # 		H00 <- NA
# # # # # # 		H10 <- NA
# # # # # # 		Div80 <- NA
# # # # # # 		Div90 <- NA
# # # # # # 		Div00 <- NA
# # # # # # 		Div10 <- NA
# # # # # # 		seg80 <- NA
# # # # # # 		seg90 <- NA
# # # # # # 		seg00 <- NA
# # # # # # 		seg10 <- NA
# # # # # # 		GEOID10 <- NA
# # # # # # 		sea.results <- NA
# # # # # # 		# unique.tr <- sort(unique(sea.bl$TRACTA))

# # # # # # 		for(i in 1:length(sea)){
# # # # # # 			seg80[i] <- list(spseg(sea[i], sea@data[,17:20]))
# # # # # # 			print(seg80)
# # # # # # 		}
# # # # # # 				slot(sea[[i]],"data")[,c("wht1980","blk1980","asi1980","his1980")]))
# # # # # # 		}

# # # # # # 			seg90[i] <- list(spseg(x=sea[[i]], data=slot(sea[[i]],"data")[,c("wht1990","blk1990","asi1990","his1990")]))
# # # # # # 			seg00[i] <- list(spseg(x=sea[[i]], data=slot(sea[[i]],"data")[,c("wht2000","blk2000","asi2000","his2000")]))
# # # # # # 			seg10[i] <- list(spseg(x=sea[[i]], data=slot(sea[[i]],"data")[,c("wht2010","blk2010","asi2010","his2010")]))
# # # # # # 			Dis80[i] <- seg80[[i]]@d
# # # # # # 			Dis90[i] <- seg90[[i]]@d
# # # # # # 			Dis00[i] <- seg00[[i]]@d
# # # # # # 			Dis10[i] <- seg10[[i]]@d
# # # # # # 			H80[i] <- seg80[[i]]@h
# # # # # # 			H90[i] <- seg90[[i]]@h
# # # # # # 			H00[i] <- seg00[[i]]@h
# # # # # # 			H10[i] <- seg10[[i]]@h
# # # # # # 			Div80[i] <- seg80[[i]]@r
# # # # # # 			Div90[i] <- seg90[[i]]@r
# # # # # # 			Div00[i] <- seg00[[i]]@r
# # # # # # 			Div10[i] <- seg10[[i]]@r
# # # # # # 			GEOID10[i] <- unique(sea[[i]]$GEOID10)
# # # # # # 			sea.results <- data.frame(GEOID10,Dis80,Dis90,Dis00,Dis10,H80,H90,H00,H10,Div80,Div90,Div00,Div10)
# # # # # # 		}

# # # # # # 		s.sort.d <- sea.results[order(-sea.results$Dissimilarity),]
# # # # # # 		s.sort.h <- sea.results[order(-sea.results$TheilsH),]
# # # # # # 		s.sort.r <- sea.results[order(-sea.results$Diversity),]

# # # # # # Dis <- NA
# # # # # # 	H <- NA
# # # # # # 	Div <- NA
# # # # # # 	seg <- NA
# # # # # # 	tracts <- NA
# # # # # # 	sea.results <- NA
# # # # # # 	# unique.tr <- sort(unique(sea.bl$TRACTA))

# # # # # # 	for(i in 1:length(sea@data)){
# # # # # # 		seg[i] <- list(spseg(sea@[[i]], data=slot(sea[[i]],"data")[,c("wht2010","blk2010","asi2010","his2010")]))
# # # # # # 		Dis[i] <- seg[[i]]@d
# # # # # # 		H[i] <- seg[[i]]@h
# # # # # # 		Div[i] <- seg[[i]]@r
# # # # # # 		tracts[i] <- unique(sea[[i]]$TRACTA)
# # # # # # # 		sea.results <- data.frame(Tracts = tracts,Dissimilarity = Dis,Diversity = Div,TheilsH = H)
# # # # # # # 	}		### Manual measures of segregation
# # # # # # 			city<-sea
# # # # # # 			# Race totals
# # # # # # 			city@data$city_Total <- sum(city@data$tot2010)
# # # # # # 			city@data$city_White <- sum(city@data$wht2010*city@data$tot2010)
# # # # # # 			city@data$city_Black <- sum(city@data$blk2010*city@data$tot2010)
# # # # # # 			# city@data$city_AI <- sum(city@data$bg_AI)
# # # # # # 			city@data$city_Asian <- sum(city@data$asi2010*city@data$tot2010)
# # # # # # 			city@data$city_Hispanic <- sum(city@data$his2010*city@data$tot2010)
# # # # # # 			# city@data$city_PacIsl <- sum(city@data$bg_PacIsl)
# # # # # # 			# city@data$city_Other <- sum(city@data$bg_Other)
# # # # # # 			# city@data$city_Two <- sum(city@data$bg_Two)

# # # # # # 			# Race proportions
# # # # # # 			city@data$city_pWhite <- city@data$city_White/city@data$city_Total
# # # # # # 			city@data$city_pBlack <- city@data$city_Black/city@data$city_Total
# # # # # # 			# city@data$city_pAI <- city@data$city_AI/city@data$city_Total
# # # # # # 			city@data$city_pAsian <- city@data$cityasi2010/city@data$city_Total
# # # # # # 			city@data$city_pPacIsl <- city@data$city_PacIsl/city@data$city_Total
# # # # # # 			city@data$city_pOther <- city@data$city_Other/city@data$city_Total
# # # # # # 			city@data$city_pTwo <- city@data$city_Two/city@data$city_Total

# # # # # # 			# Entropy
# # # # # # 			city@data$city_Ent <- city@data$city_pWhite*(log(1/city@data$city_pWhite)) +
# # # # # # 				city@data$city_pBlack*(log(1/city@data$city_pBlack)) +
# # # # # # 				city@data$city_pAsian*(log(1/city@data$city_pAsian))

# # # # # # 			city@data$city_Ent <- ifelse(is.na(city@data$city_Ent)==T,0,city@data$city_Ent)

# # # # # # 		## Block-group Specific Contribution to Dissimiarity
# # # # # # 			# Dissimilarity Index
# # # # # # 			city@data$d.wb <- .5*(abs(city@data$wht2010/city@data$city_White - city@data$blk2010/city@data$city_Black))
# # # # # # 			dissim.wb.bg <- tapply(city@data$d.wb,city@data$GISJOIN,sum,na.rm=T)

# # # # # # 			# Interaction Index
# # # # # # 			city@data$int.wb <- (city@data$blk2010/city@data$city_Black*city@data$wht2010/city@data$tot2010)
# # # # # # 			int.wb.bg <- tapply(city@data$int.wb,city@data$GISJOIN,sum,na.rm=T)
# # # # # # 			head(int.wb.bg)

# # # # # # 			# Isolation Index
# # # # # # 			city@data$iso.b <- (city@data$blk2010/city@data$city_Total*city@data$blk2010/city@data$tot2010)
# # # # # # 			isol.b.bg <- tapply(city@data$iso.b,city@data$GISJOIN,sum,na.rm=T)

# # # # # # 		### Multi-group Segregation - Theile Entropy index
# # # # # # 			city@data$bg_pWhite <- city@data$wht2010/city@data$tot2010
# # # # # # 			city@data$bg_pBlack <- city@data$blk2010/city@data$tot2010
# # # # # # 			city@data$bg_pAsian <- city@data$asi2010/city@data$tot2010

# # # # # # 			## Block level entropy
# # # # # # 			city@data$bg_ent <- city@data$bg_pWhite*(log(1/city@data$bg_pWhite)) +
# # # # # # 								city@data$bg_pBlack*(log(1/city@data$bg_pBlack)) +
# # # # # # 								city@data$bg_pAsian*(log(1/city@data$bg_pAsian))
# # # # # # 			city@data$bg_ent<- ifelse(is.na(city@data$bg_ent)==T, 0,city@data$bg_ent)

# # # # # # 			# H index
# # # # # # 			city@data$Hcalc <- (city@data$tot2010*(city@data$city_Ent-city@data$bg_ent))/(city@data$city_Total*city@data$city_Ent)
# # # # # # 			city@data$Hcalc <- ifelse(is.na(city@data$Hcalc)==T,0,city@data$Hcalc)
# # # # # # 			hindex <- unlist(tapply(city@data$Hcalc,city@data$GISJOIN,sum))

# # # # # # 		### Assemble indeces into dataframe
# # # # # # 			city_Dat <- data.frame(GISJOIN=names(unlist(dissim.wb.bg)),dissim_wb=unlist(dissim.wb.bg),isolation_b=isol.b.bg,interaction_bw=int.wb.bg,TheileH=hindex)

# # # # # # 		### Plot Histograms
# # # # # # 			par(mfrow=c(2,2))
# # # # # # 			hist(city_Dat$dissim_wb,main="White-Black Dissimilairy")
# # # # # # 			hist(city_Dat$isolation_b,main="Black Isolation")
# # # # # # 			hist(city_Dat$interaction_bw,main="White-Black Interaction")
# # # # # # 			hist(city_Dat$TheileH, main="White-Black-Asian Theile H Index")

# # # # # # 		### Join data to shapefile
# # # # # # 			city@data <- data.frame(city@data,city_Dat[match(city@data$GISJOIN,Sea_Dat$GISJOIN),])

# # # # # xx <- runif(100) # random distribution
# # # # # xx <- xx * (4000 / sum(xx))
# # # # # yy <- rep(c(40, 60), 100) # no segregation
# # # # # zz <- rep(c(100, 0), c(40, 60)) # complete segregation

# # # # # set1 <- cbind(xx, 100 - xx)
# # # # # set2 <- matrix(yy, ncol = 2, byrow = TRUE)
# # # # # set3 <- cbind(zz, 100 - zz)

# # # # # par(mar = c(5.1, 4.1, 2.1, 2.1))
# # # # # out1 <- conprof(set1, grpID = 1,
# # # # #   xlab = "Threshold level (%)",
# # # # #   ylab = "Population proportion (%)",
# # # # #   cex.lab = 0.9, cex.axis = 0.9, lty = "dotted")
# # # # # out2 <- conprof(set2, grpID = 1, add = TRUE,
# # # # #   lty = "longdash")
# # # # # out3 <- conprof(set3, grpID = 1, add = TRUE)
# # # # # title(main = paste("R =", round(out1$d, 2)))

# # # # # # shaded areas represent the summary statistic value
# # # # # if (require(graphics)) {
# # # # #   polygon(c(out1$x[1:400], 0.4, 0),
# # # # #           c(out1$y[1:400], 1, 1),
# # # # #           density = 10, angle = 60,
# # # # #           border = "transparent")
# # # # #   polygon(c(out1$x[401:999], 1, 0.4),
# # # # #           c(out1$y[401:999], 0, 0),
# # # # #           density = 10, angle = 60,
# # # # #           border = "transparent")
# # # # # }


# # # # # 	### TODO ###
# # # # # 		* Check props equal 1 on final set

# # # # # 1848+121+30+30

# # # # # 				select(SHRNHW8N, SHRNHB8N, SHRNHJ8N, SHRHSP8N, SHRNHW9N,SHRNHB9N,SHRNHA9N,SHRHSP9N, SHRNHW0N,SHRNHB0N,SHRNHA0N,SHRHSP0N, SHRNHW1N,SHRNHB1N,SHRNHA1N,SHRHSP1N,MRANHS1N) %>%
# # # # # 				# summarise(tot1980=sum(SHRNHW8N,SHRNHB8N,SHRNHJ8N,SHRHSP8N), #1980
# # # # # 				# 			wht1980=SHRNHW8N,
# # # # # 				# 			blk1980=SHRNHB8N,
# # # # # 				# 			asi1980=SHRNHJ8N,
# # # # # 				# 			his1980=SHRHSP8N,
# # # # # 				# 			tot1990=sum(SHRNHW9N,SHRNHB9N,SHRNHA9N,SHRHSP9N), #1990
# # # # # 				# 			wht1990=SHRNHW9N,
# # # # # 				# 			blk1990=SHRNHB9N,
# # # # # 				# 			asi1990=SHRNHA9N,
# # # # # 				# 			his1990=SHRHSP9N,
# # # # # 				# 			tot2000=sum(SHRNHW0N,SHRNHB0N,SHRNHA0N,SHRHSP0N), #2000
# # # # # 				# 			wht2000=SHRNHW0N,
# # # # # 				# 			blk2000=SHRNHB0N,
# # # # # 				# 			asi2000=SHRNHA0N,
# # # # # 				# 			his2000=SHRHSP0N,
# # # # # 							summarise(tot2010=sum(MRANHS1N,SHRHSP1N), #2010
# # # # # 							### MAJOR NOTE: SHRNHW1N is not in the code book from online but
# # # # # 							# IS in the dataset. The values seem realistic but need to confirm
# # # # # 							# if this is the actual variable name for non-hispanic white.
# # # # # 							wht2010=SHRNHW1N,
# # # # # 							blk2010=SHRNHB1N,
# # # # # 							asi2010=SHRNHA1N,
# # # # # 							his2010=SHRHSP1N,
# # # # # 							oth2010=(sum(SHRNHW1N,SHRNHB1N,SHRNHA1N,SHRHSP1N)-tot2010)/tot2010) #,
# # # # # 							# diffwht=wht1980-wht2010,
# # # # # 							# diffblk=blk1980-blk2010,
# # # # # 							# diffasi=asi1980-asi2010,
# # # # # 							# diffhis=his1980-his2010)
# # # wageo <- wa.tracts$GEOID10
# # # 	warace <- as.data.frame(race[race$GEO2010 %in% wageo, ]) # Subset
# # # 	glimpse(warace)

# # # 	seageo <- sea.tracts$GEOID10
# # # 	searace <- as.data.frame(race[race$GEO2010 %in% seageo, ]) # Subset
# # # 	glimpse(searace)

# # # 	### join to spatial data
# # # 	sea.tracts@data <- merge(sea.tracts@data,searace,by.x="GEOID10",by.y="GEO2010")
# # # 	sea.tracts@data <- merge(sea.tracts@data,dis_index, by.x="GEOID10", by.y="GEOID10")
# # # 	glimpse(sea.tracts@data)

# # # 	wa.tracts@data <- merge(wa.tracts@data,warace,by.x="GEOID10",by.y="GEO2010")
# # # 	# wa.tracts@data <- merge(wa.tracts@data,dis_index, by.x="GEOID10", by.y="GEOID10")
# # # 	glimpse(wa.tracts@data)

# # # ### Seattle level total population and proportions
# # # 	sea.tracts$sea_tot80 <- sum(sea.tracts$t1980)
# # # 	sea.tracts$sea_wh_tot80 <- sum(sea.tracts$nw1980)
# # # 	sea.tracts$sea_bl_tot80 <- sum(sea.tracts$nb1980)
# # # 	sea.tracts$sea_ot_tot80 <- sum(sea.tracts$no1980)

# # # 	sea.tracts$sea_pwh_tot80 <- sea.tracts$sea_wh_tot80/sea.tracts$sea_tot80
# # # 	sea.tracts$sea_pbl_tot80 <- sea.tracts$sea_bl_tot80/sea.tracts$sea_tot80
# # # 	sea.tracts$sea_pot_tot80 <- sea.tracts$sea_ot_tot80/sea.tracts$sea_tot80

# # # 	sea.tracts$sea_tot90 <- sum(sea.tracts$t1990)
# # # 	sea.tracts$sea_wh_tot90 <- sum(sea.tracts$nw1990)
# # # 	sea.tracts$sea_bl_tot90 <- sum(sea.tracts$nb1990)
# # # 	sea.tracts$sea_ot_tot90 <- sum(sea.tracts$no1990)

# # # 	sea.tracts$sea_pwh_tot90 <- sea.tracts$sea_wh_tot90/sea.tracts$sea_tot90
# # # 	sea.tracts$sea_pbl_tot90 <- sea.tracts$sea_bl_tot90/sea.tracts$sea_tot90
# # # 	sea.tracts$sea_pot_tot90 <- sea.tracts$sea_ot_tot90/sea.tracts$sea_tot90

# # # 	sea.tracts$sea_tot00 <- sum(sea.tracts$t2000)
# # # 	sea.tracts$sea_wh_tot00 <- sum(sea.tracts$nw2000)
# # # 	sea.tracts$sea_bl_tot00 <- sum(sea.tracts$nb2000)
# # # 	sea.tracts$sea_ot_tot00 <- sum(sea.tracts$no2000)

# # # 	sea.tracts$sea_pwh_tot00 <- sea.tracts$sea_wh_tot00/sea.tracts$sea_tot00
# # # 	sea.tracts$sea_pbl_tot00 <- sea.tracts$sea_bl_tot00/sea.tracts$sea_tot00
# # # 	sea.tracts$sea_pot_tot00 <- sea.tracts$sea_ot_tot00/sea.tracts$sea_tot00

# # # 	sea.tracts$sea_tot10 <- sum(sea.tracts$t2010)
# # # 	sea.tracts$sea_wh_tot10 <- sum(sea.tracts$nw2010)
# # # 	sea.tracts$sea_bl_tot10 <- sum(sea.tracts$nb2010)
# # # 	sea.tracts$sea_ot_tot10 <- sum(sea.tracts$no2010)

# # # 	sea.tracts$sea_pwh_tot10 <- sea.tracts$sea_wh_tot10/sea.tracts$sea_tot10
# # # 	sea.tracts$sea_pbl_tot10 <- sea.tracts$sea_bl_tot10/sea.tracts$sea_tot10
# # # 	sea.tracts$sea_pot_tot10 <- sea.tracts$sea_ot_tot10/sea.tracts$sea_tot10

# # # 	glimpse(sea.tracts@data)

# # # ### Segregation indices


# # # ### Dissimilarity ###
# # # 	# tract specific contribution to dissimilairty
# # # 	sea.tracts$d.wb1980 <- .5*(abs(sea.tracts$nw1980/sea.tracts$sea_wh_tot80 - sea.tracts$nb1980/sea.tracts$sea_bl_tot80))
# # # 	sea.tracts$d.wb1990 <- .5*(abs(sea.tracts$nw1990/sea.tracts$sea_wh_tot90 - sea.tracts$nb1990/sea.tracts$sea_bl_tot90))
# # # 	sea.tracts$d.wb2000 <- .5*(abs(sea.tracts$nw2000/sea.tracts$sea_wh_tot00 - sea.tracts$nb2000/sea.tracts$sea_bl_tot00))
# # # 	sea.tracts$d.wb2010 <- .5*(abs(sea.tracts$nw2010/sea.tracts$sea_wh_tot10 - sea.tracts$nb2010/sea.tracts$sea_bl_tot10))

# # # 	# City wide dissimilarity
# # # 	sea.tracts$sea_d.wb1980 <- sum(sea.tracts$d.wb1980)
# # # 	sea.tracts$sea_d.wb1990 <- sum(sea.tracts$d.wb1990)
# # # 	sea.tracts$sea_d.wb2000 <- sum(sea.tracts$d.wb2000)
# # # 	sea.tracts$sea_d.wb2010 <- sum(sea.tracts$d.wb2010)

# # # ### Interaction Index ###
# # # 	# tract interaction
# # # 	sea.tracts$int.wb1980 <- (sea.tracts$nb1980/sea.tracts$sea_bl_tot80 * sea.tracts$nw1980/sea.tracts$t1980)
# # # 	sea.tracts$int.wb1990 <- (sea.tracts$nb1990/sea.tracts$sea_bl_tot90 * sea.tracts$nw1990/sea.tracts$t1990)
# # # 	sea.tracts$int.wb2000 <- (sea.tracts$nb2000/sea.tracts$sea_bl_tot00 * sea.tracts$nw2000/sea.tracts$t2000)
# # # 	sea.tracts$int.wb2010 <- (sea.tracts$nb2010/sea.tracts$sea_bl_tot10 * sea.tracts$nw2010/sea.tracts$t2010)

# # # 	# city interaction
# # # 	sea.tracts$sea_int.wb1980 <- sum(sea.tracts$int.wb1980)
# # # 	sea.tracts$sea_int.wb1990 <- sum(sea.tracts$int.wb1990)
# # # 	sea.tracts$sea_int.wb2000 <- sum(sea.tracts$int.wb2000)
# # # 	sea.tracts$sea_int.wb2010 <- sum(sea.tracts$int.wb2010)

# # # 	glimpse(sea.tracts)

# # # ### Isolation ###
# # # 	# Tract level isolation
# # # 	sea.tracts$iso.b1980 <- (sea.tracts$nb1980/sea.tracts$sea_bl_tot80 * sea.tracts$nb1980/sea.tracts$t1980)
# # # 	sea.tracts$iso.b1990 <- (sea.tracts$nb1990/sea.tracts$sea_bl_tot90 * sea.tracts$nb1990/sea.tracts$t1990)
# # # 	sea.tracts$iso.b2000 <- (sea.tracts$nb2000/sea.tracts$sea_bl_tot00 * sea.tracts$nb2000/sea.tracts$t2000)
# # # 	sea.tracts$iso.b2010 <- (sea.tracts$nb2010/sea.tracts$sea_bl_tot10 * sea.tracts$nb2010/sea.tracts$t2010)

# # # 	# City level isolation
# # # 	sea.tracts$sea_iso.b1980 <- sum(sea.tracts$iso.b1980)
# # # 	sea.tracts$sea_iso.b1990 <- sum(sea.tracts$iso.b1990)
# # # 	sea.tracts$sea_iso.b2000 <- sum(sea.tracts$iso.b2000)
# # # 	sea.tracts$sea_iso.b2010 <- sum(sea.tracts$iso.b2010)

# # # 	glimpse(sea.tracts)

# # # ### Entropy ###
# # # 	# Tract level entropy
# # # 	sea.tracts$tr_ent80<-sea.tracts$pw1980*(log(1/sea.tracts$pw1980))+
# # # 				sea.tracts$pb1980*(log(1/sea.tracts$pb1980))+
# # # 				sea.tracts$pa1980*(log(1/sea.tracts$pa1980))+
# # # 				sea.tracts$ph1980*(log(1/sea.tracts$ph1980))+
# # # 				ifelse(is.na(sea.tracts$po1980*(log(1/sea.tracts$po1980)))==T, 0, sea.tracts$po1980*(log(1/sea.tracts$po1980)))
# # # 	sea.tracts$tr_ent80<-ifelse(is.na(sea.tracts$tr_ent80)==T, 0,sea.tracts$tr_ent80)

# # # 	sea.tracts$tr_ent90<-sea.tracts$pw1990*(log(1/sea.tracts$pw1990))+
# # # 				sea.tracts$pb1990*(log(1/sea.tracts$pb1990))+
# # # 				sea.tracts$pa1990*(log(1/sea.tracts$pa1990))+
# # # 				sea.tracts$ph1990*(log(1/sea.tracts$ph1990))+
# # # 				sea.tracts$po1990*(log(1/sea.tracts$po1990))
# # # 	sea.tracts$tr_ent90<-ifelse(is.na(sea.tracts$tr_ent90)==T, 0,sea.tracts$tr_ent90)

# # # 	sea.tracts$tr_ent00<-sea.tracts$pw2000*(log(1/sea.tracts$pw2000))+
# # # 				sea.tracts$pb2000*(log(1/sea.tracts$pb2000))+
# # # 				sea.tracts$pa2000*(log(1/sea.tracts$pa2000))+
# # # 				sea.tracts$ph2000*(log(1/sea.tracts$ph2000))+
# # # 				sea.tracts$po2000*(log(1/sea.tracts$po2000))
# # # 	sea.tracts$tr_ent00<-ifelse(is.na(sea.tracts$tr_ent00)==T, 0,sea.tracts$tr_ent00)

# # # 	sea.tracts$tr_ent10<-sea.tracts$pw2010*(log(1/sea.tracts$pw2010))+
# # # 				sea.tracts$pb2010*(log(1/sea.tracts$pb2010))+
# # # 				sea.tracts$pa2010*(log(1/sea.tracts$pa2010))+
# # # 				sea.tracts$ph2010*(log(1/sea.tracts$ph2010))+
# # # 				sea.tracts$po2010*(log(1/sea.tracts$po2010))
# # # 	sea.tracts$tr_ent10<-ifelse(is.na(sea.tracts$tr_ent10)==T, 0,sea.tracts$tr_ent10)

# # # 	# Seattle level Entropy
# # # 	sea.tracts$sea_ent80<-sea.tracts$sea_pwh_tot80*(log(1/sea.tracts$sea_pwh_tot80))+
# # # 				sea.tracts$sea_pbl_tot80*(log(1/sea.tracts$sea_pbl_tot80))+
# # # 				sea.tracts$sea_pas_tot80*(log(1/sea.tracts$sea_pas_tot80))+
# # # 				sea.tracts$sea_phi_tot80*(log(1/sea.tracts$sea_phi_tot80))+
# # # 				sea.tracts$sea_pot_tot80*(log(1/sea.tracts$sea_pot_tot80))
# # # 	sea.tracts$sea_ent80<-ifelse(is.na(sea.tracts$sea_ent80)==T, 0,sea.tracts$sea_ent80)

# # # 	sea.tracts$sea_ent90<-sea.tracts$sea_pwh_tot90*(log(1/sea.tracts$sea_pwh_tot90))+
# # # 				sea.tracts$sea_pbl_tot90*(log(1/sea.tracts$sea_pbl_tot90))+
# # # 				sea.tracts$sea_pas_tot90*(log(1/sea.tracts$sea_pas_tot90))+
# # # 				sea.tracts$sea_phi_tot90*(log(1/sea.tracts$sea_phi_tot90))+
# # # 				sea.tracts$sea_pot_tot90*(log(1/sea.tracts$sea_pot_tot90))
# # # 	sea.tracts$sea_ent90<-ifelse(is.na(sea.tracts$sea_ent90)==T, 0,sea.tracts$sea_ent90)

# # # 	sea.tracts$sea_ent00<-sea.tracts$sea_pwh_tot00*(log(1/sea.tracts$sea_pwh_tot00))+
# # # 				sea.tracts$sea_pbl_tot00*(log(1/sea.tracts$sea_pbl_tot00))+
# # # 				sea.tracts$sea_pas_tot00*(log(1/sea.tracts$sea_pas_tot00))+
# # # 				sea.tracts$sea_phi_tot00*(log(1/sea.tracts$sea_phi_tot00))+
# # # 				sea.tracts$sea_pot_tot00*(log(1/sea.tracts$sea_pot_tot00))
# # # 	sea.tracts$sea_ent00<-ifelse(is.na(sea.tracts$sea_ent00)==T, 0,sea.tracts$sea_ent00)

# # # 	sea.tracts$sea_ent10<-sea.tracts$sea_pwh_tot10*(log(1/sea.tracts$sea_pwh_tot10))+
# # # 				sea.tracts$sea_pbl_tot10*(log(1/sea.tracts$sea_pbl_tot10))+
# # # 				sea.tracts$sea_pas_tot10*(log(1/sea.tracts$sea_pas_tot10))+
# # # 				sea.tracts$sea_phi_tot10*(log(1/sea.tracts$sea_phi_tot10))+
# # # 				sea.tracts$sea_pot_tot10*(log(1/sea.tracts$sea_pot_tot10))
# # # 	sea.tracts$sea_ent10<-ifelse(is.na(sea.tracts$sea_ent10)==T, 0,sea.tracts$sea_ent10)

# # # 	# Tract contribution to city wide entropy
# # # 	sea.tracts$Hcalc1980<-(sea.tracts$t1980*(sea.tracts$sea_ent80-sea.tracts$tr_ent80))/(sea.tracts$sea_tot80*sea.tracts$sea_ent80)
# # # 	sea.tracts$Hcalc1980 <- ifelse(is.na(sea.tracts$Hcalc1980)==T, 0, sea.tracts$Hcalc1980)
# # # 	hist(sea.tracts$Hcalc1980)

# # # 	sea.tracts$Hcalc1990<-(sea.tracts$t1990*(sea.tracts$sea_ent90-sea.tracts$tr_ent90))/(sea.tracts$sea_tot90*sea.tracts$sea_ent90)
# # # 	sea.tracts$Hcalc1990 <- ifelse(is.na(sea.tracts$Hcalc1990)==T, 0, sea.tracts$Hcalc1990)
# # # 	hist(sea.tracts$Hcalc1990)

# # # 	sea.tracts$Hcalc2000<-(sea.tracts$t2000*(sea.tracts$sea_ent00-sea.tracts$tr_ent00))/(sea.tracts$sea_tot00*sea.tracts$sea_ent00)
# # # 	sea.tracts$Hcalc2000 <- ifelse(is.na(sea.tracts$Hcalc2000)==T, 0, sea.tracts$Hcalc2000)
# # # 	hist(sea.tracts$Hcalc2000)

# # # 	sea.tracts$Hcalc2010<-(sea.tracts$t2010*(sea.tracts$sea_ent10-sea.tracts$tr_ent10))/(sea.tracts$sea_tot10*sea.tracts$sea_ent10)
# # # 	sea.tracts$Hcalc2010 <- ifelse(is.na(sea.tracts$Hcalc2010)==T, 0, sea.tracts$Hcalc2010)
# # # 	hist(sea.tracts$Hcalc2010)

# # # 	# Seattle Thield H index
# # # 	sea.tracts$Hindex1980 <- sum(sea.tracts$Hcalc1980)
# # # 	sea.tracts$Hindex1990 <- sum(sea.tracts$Hcalc1990)
# # # 	sea.tracts$Hindex2000 <- sum(sea.tracts$Hcalc2000)
# # # 	sea.tracts$Hindex2010 <- sum(sea.tracts$Hcalc2010)

# # # 	glimpse(sea.tracts)

# # # ### Subset data with only relevent data ###
# # # 	sea.f <- sea.tracts
# # # 	sea.f@data <- sea.f@data[,c(-2:-12,-14,-15,-64:-107,-132:-139)]
# # # ### NOTE: Figure out the two entropy seattle level indicies that
# # # 	glimpse(sea.f)

# # 		# dis <- sea.f@data %>%
# # 		# 	group_by(GEOID10) %>%
# # 		# 	select(dis1980,dis1990,dis2000,dis2010) %>%
# # 		# 	gather(year, meas, -GEOID10) %>% 	# reshape wide to long
# # 		# 	separate(year,into=c("dis","year"),sep=3) %>%
# # 		# 	arrange(GEOID10)

# # 	# Attempt 1
# # 	# sea.data %>%
# # 	# 	select(GEO2010,dis1980:dis2010,pwt1980:pot1980,pwt1990:pot1990,pwt2000:pot2000, pwt2010:pot2010) %>%
# # 	# 	gather(year,dis,pwt,pbl,pot) %>%
# # 	# 	seperate
# # 	# 	seperate(year, into=c("dis","year"),sep=3)
# # 	# 	gather(year,meas,-GEO2010) %>%
# # 	# 	seperate(year,)

# # 	# 	group_by(GEO2010) %>%
# # 	# 	select(contains("pwt"),contains("pbl"),contains("pot")) %>%
# # 	# 	gather(year,pwt,pbl,pot) %>%
# # 	# 	seperate()

# # 	# 	d.lcmm <- d.prop %>%
# # 	# 				group_by(GEO2010) %>%
# # 	# 				select(everything()) %>%
# # 	# 				gather()
# # 	# # Long format
# # 	# d.prop.l <- d.prop %>%
# # 	# 			gather
# # 	# 			group_by(GEO2010) %>%
# # 	# 			select(everything()) %>%
# # 	# 			gather(year,meas,-GEO2010) %>%
# # 	# 			separate(year, into=c("dis","year"),sep=3) %>%
# # 	# 			arrange(GEO2010)

# # 	# # Attempt 2
# # 	# reshape(sea.data, direction="long",
# # 	# 		varying=2:33,
# # 	# 		timevar="Year",
# # 	# 		times=c("1980","1990","2000","2010"),
# # 	# 		v.names=c("dis","pwt","pbl","pot"),
# # 	# 		idvar="GEO2010")
# # 	# # Attempt 3
# # 	# long.sea.data <- sea.data %>%
# # 	# 		gather(v,value,dis1980:pot2010) %>%
# # 	# 		separate(v,c("var","year"),sep=3)%>%
# # 	# 		arrange(GEO2010) %>%
# # 	# 		spread(var,value)

# # 	# 		dw <- read.table(header=T, text='
# # 	# 		 sbj f1.avg f1.sd f2.avg f2.sd  blabla
# # 	# 		   A   10    6     50     10      bA
# # 	# 		   B   12    5     70     11      bB
# # 	# 		   C   20    7     20     8       bC
# # 	# 		   D   22    8     22     9       bD
# # 	# 		 ')

# # 	# 		dw %>%
# # 	# 		  gather(v, value, f1.avg:f2.sd) %>%
# # 	# 		  separate(v, c("var", "col")) %>%
# # 	# 		  arrange(sbj) %>%
# # 	# 		  spread(col, value)

# 	# # attemp 1
# 	# dbo3.sp <- dbo3 %>% group_by(GEO2010)
# 	# sea.lcmm <- sea.tracts
# 	# sea.lcmm@data <- left_join(sea.lcmm@data,dbo3.sp, by = c("GEOID10" = "GEO2010"))
# 	# plot(sea.lcmm,col=sea.lcmm@data$dbo3)
# 	# text(sea.lcmm@data$dbo3)
# 	# labels(name=sea.lcmm@data$dbo3)

# ### Test lcmm
# # t1 <- lcmm(dis~year,subject='GEO2010',mixture=~year,ng=5,idiag=T,data=l.dt,link="linear")
# # ### Multlcmm is broken, start here for fix

# # tm1 <- mlcmm(dis+pbl~year,random=~year,subject="GEO2010",randomY=T,link="linear",data=l.dt)

# # m2 <- multlcmm(dis+pbl~1+year,random=~1+year,subject="GEO2010",link="linear",ng=2,mixture=~1+year,data=l.dt)

# # m2 <-
# # 		multlcmm(dis+pbl~1+year,
# # 				random=~1+year,
# # 				subject="GEO2010",
# # 				link="linear",
# # 				ng=2,
# # 				mixture=~1+year,
# # 				# classmb=~1+year,
# # 				data=l.dt)

# # ### End fix

# # 	## multlcmm
# # 	# Attempt 1
# # 	s1 <- multlcmm(dis+pot+pbl~year,
# # 				random=~year,
# # 				# ng=2,
# # 				subject="GEO2010",
# # 				randomY=TRUE,
# # 				link="4-quant-splines",
# # 				data=l.dt)

# 	# summary(s1)
# 	# postprob(s1)
# 	# plot(s1,which="linkfunction")

# 	# s2 <- multlcmm(dis+pbl~1+year,
# 	# 			random=~1+year,
# 	# 			ng=2,
# 	# 			subject="GEO2010",
# 	# 			randomY=TRUE,
# 	# 			link="4-quant-splines",
# 	# 			data=l.dt)

# 	# summary(s2)
# 	# postprob(s2)
# 	# plot(s2,which="linkfunction")


# 	# s3 <- multlcmm(dis+pbl+pot~1+year,
# 	# 			random=~1+year,
# 	# 			# ng=2,
# 	# 			subject="GEO2010",
# 	# 			randomY=TRUE,
# 	# 			link="4-quant-splines",
# 	# 			data=l.dt)

# 	# summary(s3)
# 	# postprob(s3)
# 	# plot(s3,which="linkfunction")