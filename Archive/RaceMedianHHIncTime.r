# ==========================================================================
# AMI Plots
# ==========================================================================

rm(list=ls()) #reset
options(scipen=10) # avoid scientific notation
gc()

library(colorout)
library(lubridate)
library(tidycensus)
options(tigris_use_cache = TRUE)
library(tidyverse)

# ==========================================================================
# Data
# ==========================================================================
load("/Users/timothythomas/Academe/Data/Zillow/RData/zillow.rdata")

# ==========================================================================
# Racial economic stratification
# ==========================================================================
	acsvars <- c(medhhinc = "B19013_001",
				 white = "B19013A_001",
				 black = "B19013B_001",
				 aian = "B19013C_001",
				 asian = "B19013D_001",
				 nhop = "B19013E_001",
				 other = "B19013F_001",
				 two = "B19013G_001",
				 whitenl = "B19013H_001",
				 latinx = "B19013I_001")

### Median Income 2016 ACS 1-year ###
	acs16 <- get_acs(geography = "place",
				   variables = acsvars,
				   cache_table = TRUE,
				   year = 2016,
				   output = "tidy",
				   # state = "WA",
				   # county = "King",
				   geometry = FALSE,
				   survey = "acs1") %>%
			filter(GEOID == "5363000") %>% # place = Seattle
			mutate(year = 2016,
				   estimate = estimate*1.02) # 2018 dollars CPI

	acs15 <- get_acs(geography = "place",
				   variables = acsvars,
				   cache_table = TRUE,
				   year = 2015,
				   output = "tidy",
				   # state = "WA",
				   # county = "King",
				   geometry = FALSE,
				   survey = "acs1") %>%
			filter(GEOID == "5363000") %>%
			mutate(year = 2015,
				   estimate = estimate*1.05)

	acs14 <- get_acs(geography = "place",
				   variables = acsvars,
				   cache_table = TRUE,
				   year = 2014,
				   output = "tidy",
				   # state = "WA",
				   # county = "King",
				   geometry = FALSE,
				   survey = "acs1") %>%
			filter(GEOID == "5363000") %>%
			mutate(year = 2014,
				   estimate = estimate*1.05)

	acs13 <- get_acs(geography = "place",
				   variables = acsvars,
				   cache_table = TRUE,
				   year = 2013,
				   output = "tidy",
				   # state = "WA",
				   # county = "King",
				   geometry = FALSE,
				   survey = "acs1") %>%
			filter(GEOID == "5363000") %>%
			mutate(year = 2013,
				   estimate = estimate*1.07)

	acs12 <- get_acs(geography = "place",
				   variables = acsvars,
				   cache_table = TRUE,
				   year = 2012,
				   output = "tidy",
				   # state = "WA",
				   # county = "King",
				   geometry = FALSE,
				   survey = "acs1") %>%
			filter(GEOID == "5363000") %>%
			mutate(year = 2012,
				   estimate = estimate*1.08)


	acs10 <- get_acs(geography = "place",
				   variables = acsvars,
				   cache_table = TRUE,
				   year = 2010,
				   output = "tidy",
				   # state = "WA",
				   # county = "King",
				   geometry = FALSE,
				   survey = "acs5") %>%
			filter(GEOID == "5363000") %>%
			mutate(year = 2007,
				   estimate = estimate*1.21)

	#
	# 1 year ACS not available before 2012
	#

### Decnial Censuses

	decvars00 <- c(medianinc = "P053001",
				   white = "P152A001",
				   black = "P152B001",
				   aian = "P152C001",
				   asian = "P152D001",
				   nhop = "P152E001",
				   other = "P152F001",
				   two = "P152G001",
				   latinx = "P152H001",
				   whitenl = "P152I001")

	dec00 <- get_decennial(geography = "county",
						   variables = decvars00,
						   cache_table = TRUE,
						   year = 2000,
						   sumfile = "sf1",
						   state = "WA",
						   county = "King",
						   geometry = FALSE,
						   output = "tidy") %>%
			 mutate(year = 2000,
			 		value = value*1.50) %>%
			 rename(estimate = value)


	decvars90 <- c(medianinc = "P080A001")
	dec90 <- get_decennial(geography = "county",
						   variables = decvars90,
						   cache_table = TRUE,
						   year = 1990,
						   sumfile = "sf1",
						   state = "WA",
						   county = "King",
						   geometry = FALSE,
						   output = "tidy") %>%
			 mutate(year = 1990,
			 		value = value*2.03) %>%
			 rename(estimate = value)

### Bind df together
	hud <- read.table(header=T, text='
	GEOID	NAME	variable	estimate	moe	year
	53033	King	medianinc	103400		NA	2018
	53033	King	medianinc	97920		NA	2017
	', stringsAsFactors = FALSE) %>%
	mutate(GEOID = as.character(GEOID))

	econ <- bind_rows(hud,
					  acs16,
					  # acs15, # 2015 and 2013 look odd so removing
					  acs14,
					  # acs13,
					  acs12,
					  acs10,
					  dec00)

	races <- c("medianinc", "asian", "black", "latinx", "whitenl")
	econ.sub <- econ %>%
				filter(variable %in% races) %>%
				mutate(variable = factor(variable,
										 levels = c("medianinc",
										 			"whitenl",
										 			"asian",
										 			"latinx",
										 			"black")))

	econ.pov <- econ %>%
				filter(variable == "medianinc") %>%
				mutate(low = .8*estimate,
					   vlow = .5*estimate,
					   exlow = .3*estimate) %>%
				gather(pov, value, low:exlow) %>%
				mutate(pov = factor(pov, levels = c("low", "vlow","exlow"))) %>%
				arrange(desc(year))

### Plot ###

medinc <- ggplot() +
		  geom_smooth(data = econ.pov %>% filter(pov =="low"),
				aes(y = value,
					x = year,
					color = pov),
				color = "grey60",
				se = FALSE,
				# linetype = "F1",
				alpha = .5,
				size = 3,) +
		  geom_smooth(data = econ.pov %>% filter(pov =="vlow"),
				aes(y = value,
					x = year,
					color = pov),
				color = "grey60",
				se = FALSE,
				# linetype = "F1",
				alpha = .5,
				size = 3,) +
		  geom_smooth(data = econ.pov %>% filter(pov =="exlow"),
				aes(y = value,
					x = year,
					color = pov),
				color = "grey60",
				se = FALSE,
				# linetype = "F1",
				alpha = .5,
				size = 3,) +
		  geom_line(data = econ.sub,
					  aes(y = estimate,
						  x = year,
						  color = variable),
					  size = .8
					  ) +
		  geom_point(data = econ.sub,
					  aes(y = estimate,
						  x = year,
						  color = variable),
					  alpha = .5) +
		  theme_minimal() +
		  # theme(legend.position='bottom') +
		  theme(axis.text.x = element_text(angle = -45, hjust = 0)) +
		  scale_color_brewer(palette = "Set1",
							    direction=-1,
							    name = "Household\nMedian Income",
							    breaks=c("medianinc",
										 "whitenl",
										 "asian",
										 "latinx",
										 "black"),
							    labels=c("Overall",
										 "NL White",
										 "Asian",
										 "Latinx",
										 "African American")) +
			labs(#title = "King County Racial and Ethnic Differences\nin Median  Household Income 2000 - 2016",
				  y = "2018 Dollars",
				  x = "Year") +
			ylim(20000, 104000) +
			scale_x_continuous(breaks=c(2000,2007,2012,2014,2016,2018))

medinc

# ggsave(filename = "Sync/Academe/Presentations/180507_CityTalk/SeaRaceIncDiff_Tiers.pdf",
	   # width = 7,
	   # height = 3.5)

# v00 <- load_variables(2000, "sf3", cache = TRUE)
# View(v00)

# ==========================================================================
# Rent through time
# ==========================================================================

### Seattle tracts ###
	sea <- sf::st_read("/Users/timothythomas/Academe/Data/USCensus/Shapefiles/2010/Washington/Seattle/SeaTr2010.shp")

	rent <- z.rent %>%
			mutate(GEOID10 = factor(GEO2010),
		   		   date.rent = ymd(date.rent),
		   		   year = year(date.rent)) %>%
			filter(GEOID10 %in% sea$GEOID10) %>%
			group_by(year) %>%
			summarize(rent = median(ZRI, na.rm = T))
	glimpse(rent)

	rb <- rent %>%
		  mutate(rb30 = (rent*12)/.3,
		  		 rb50 = (rent*12)/.5)

# Plot
	rbplot <- ggplot() +
		  geom_smooth(data = econ.pov %>% filter(pov =="low"),
				aes(y = value,
					x = year,
					color = pov),
				color = "grey80",
				se = FALSE,
				# linetype = "F1",
				alpha = .5,
				size = 3,) +
		  geom_smooth(data = econ.pov %>% filter(pov =="vlow"),
				aes(y = value,
					x = year,
					color = pov),
				color = "grey80",
				se = FALSE,
				# linetype = "F1",
				alpha = .5,
				size = 3,) +
		  geom_smooth(data = econ.pov %>% filter(pov =="exlow"),
				aes(y = value,
					x = year,
					color = pov),
				color = "grey80",
				se = FALSE,
				# linetype = "F1",
				alpha = .5,
				size = 3,) +
		  geom_line(data = econ.sub,
					  aes(y = estimate,
						  x = year,
						  color = variable),
					  size = .8
					  ) +
		  geom_point(data = econ.sub,
					  aes(y = estimate,
						  x = year,
						  color = variable),
					  alpha = .5) +
		  geom_smooth(data = rb,
		  			  aes(y = rb30,
		  			  	  x = year),
		  			  color = "red",
		  			  linetype = "dashed",
		  			  se = FALSE) +
		  geom_smooth(data = rb,
		  			  aes(y = rb50,
		  			  	  x = year),
		  			  color = "blue",
		  			  linetype = "dashed",
		  			  se = FALSE) +
		  theme_minimal() +
		  # theme(legend.position='bottom') +
		  theme(axis.text.x = element_text(angle = -45, hjust = 0)) +
		  scale_color_brewer(palette = "Set1",
							    direction=-1,
							    name = "Household\nMedian Income",
							    breaks=c("medianinc",
										 "whitenl",
										 "asian",
										 "latinx",
										 "black"),
							    labels=c("Overall",
										 "NL White",
										 "Asian",
										 "Latinx",
										 "African American")) +
			labs(#title = "King County Racial and Ethnic Differences\nin Median  Household Income 2000 - 2016",
				  y = "2018 Dollars",
				  x = "Year") +
			ylim(20000, 104000) +
			scale_x_continuous(breaks=c(2000,2007,2012,2014,2016,2018))


ggsave(filename = "/Users/timothythomas/Academe/Presentations/180604_RentersCommissionTalk.pdf",
	   width = 7,
	   height = 3.5)


# ==========================================================================
# HUD Values
# ==========================================================================

hud <- read.delim(file = "H_Drive/Temp/NCP/Data/HUD/combined_years_HUD_AMI.txt")


# ==========================================================================
# NEXT:
# * add group specific income values (how many at what economic level) to
# show who falls bove and below rent burden.
# ==========================================================================
